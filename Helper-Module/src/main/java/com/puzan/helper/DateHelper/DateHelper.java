/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.helper.DateHelper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author puzan
 */
public class DateHelper {

    public static DateHelper newInstance() {
        return new DateHelper();
    }

    public String formatDateMonthDay(String date) {
        String formattedDate = null;
        try {
            //month and day

            DateFormat parser = new SimpleDateFormat("yyyy-MM-dd");
            Date newDate = (Date) parser.parse(date);

            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
            formattedDate = sdf.format(newDate);

        } catch (ParseException ex) {
            Logger.getLogger(DateHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return formattedDate;
    }

    public Date getFirstDate() {
        Calendar lastDate = Calendar.getInstance();
        lastDate.set(Calendar.DAY_OF_MONTH, lastDate.getActualMinimum(Calendar.DAY_OF_MONTH));
        return lastDate.getTime();
    }

    public Date getLasttDate() {
        Calendar firstDate = Calendar.getInstance();
        firstDate.set(Calendar.DAY_OF_MONTH, firstDate.getActualMaximum(Calendar.DAY_OF_MONTH));
        return firstDate.getTime();
    }

    public Date stringToDate(String date) {
        DateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date formatterDate = null;
        try {
            formatterDate = (Date) parser.parse(date);
        } catch (ParseException ex) {
            Logger.getLogger(DateHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return formatterDate;
    }

    public String dateToString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }
}
