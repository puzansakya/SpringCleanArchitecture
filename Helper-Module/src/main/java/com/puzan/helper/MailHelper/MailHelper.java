/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.helper.MailHelper;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author puzan
 */
public class MailHelper {

    public static MailHelper newInstance() {
        return new MailHelper();
    }

    public void sendMail(String sender,String receipient, String subject, String content) {
        String to = receipient;
        String from = sender;
        String host = "smtp.wlink.com.np";

        //Get session object
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", host);
        Session session = Session.getDefaultInstance(properties);

        //compose the message
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            message.setText(content);

            //send message
            Transport.send(message);
            System.out.println("message sent successfully....");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
