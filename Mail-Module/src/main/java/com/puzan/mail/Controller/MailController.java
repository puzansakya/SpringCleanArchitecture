/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.mail.Controller;

import com.puzan.entity.Entities.Mail;
import com.puzan.entity.Entities.User;
import com.puzan.helper.MailHelper.MailHelper;
import com.puzan.mail.DTO.MailDTO;
import com.puzan.mail.Service.MailService;
import com.puzan.user.Service.UserService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzan
 */
@Controller
@CrossOrigin
@RequestMapping(value = "admin/mail")
public class MailController {

    @Autowired
    private MailService ms;
    @Autowired
    private UserService us;
   
    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("active", "mail");
        return "Mail/index";
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<MailDTO> getAll() {
        List<MailDTO> mailDTOList = new ArrayList<>();
        for (Mail mail : ms.getAll()) {
            MailDTO mDTO = new MailDTO();
            mDTO.setMailId(mail.getMailId());
            mDTO.setSubject(mail.getSubject());
            mDTO.setMessage(mail.getMessage());
            mDTO.setSendTo(mail.getSendTo());
            mDTO.setIsDelete(mail.getIsDelete());
            mDTO.setSendBy(mail.getSendBy().getUsername());
            mDTO.setSendDate(mail.getSendDate());
            mailDTOList.add(mDTO);
        }
        return mailDTOList;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public MailDTO save(@RequestBody MailDTO mailDTO) {
      
        String receipient = mailDTO.getSendTo();
        String subject = mailDTO.getSubject();
        String message = mailDTO.getMessage();
        User user = us.getUserByUsername(mailDTO.getSendBy());        
        Boolean isDelete = mailDTO.isIsDelete();

        //mail sending preocess
        //check if the mail is sent
        //currently the sender is hardcoded
        //in production get the sender email from loggedin user
        MailHelper.newInstance().sendMail("puzansakya@gmail.com", receipient, subject, message);

        Mail mail = new Mail();        
        mail.setSubject(subject);
        mail.setMessage(message);
        mail.setSendTo(receipient);
        mail.setIsDelete(isDelete);
        mail.setSendBy(user);

        Mail retMail = ms.insert(mail);

        MailDTO retMailDTO = new MailDTO();
        retMailDTO.setMailId(retMail.getMailId());
        retMailDTO.setSubject(retMail.getSubject());
        retMailDTO.setMessage(retMail.getMessage());
        retMailDTO.setSendTo(retMail.getSendTo());
        retMailDTO.setIsDelete(retMail.getIsDelete());
        retMailDTO.setSendBy(retMail.getSendBy().getUsername());

        return retMailDTO;
    }
}
