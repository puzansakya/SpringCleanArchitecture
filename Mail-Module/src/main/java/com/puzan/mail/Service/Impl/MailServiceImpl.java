/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.mail.Service.Impl;

import com.puzan.entity.Entities.Mail;
import com.puzan.mail.DAO.MailDAO;
import com.puzan.mail.Service.MailService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "MailService")
public class MailServiceImpl implements MailService {

    @Autowired
    private MailDAO mailDAO;

    @Override
    public List<Mail> getAll() {
        return mailDAO.getAll();
    }

    @Override
    public Mail insert(Mail t) {
        return mailDAO.insert(t);
    }

    @Override
    public void delete(int id) {
        mailDAO.delete(id);
    }

    @Override
    public void update(Mail t) {
        mailDAO.update(t);
    }

    @Override
    public Mail getById(int id) {
        return mailDAO.getById(id);
    }
}
