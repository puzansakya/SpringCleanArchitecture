/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.mail.DAO.Impl;

import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.entity.Entities.Mail;
import com.puzan.mail.DAO.MailDAO;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "MailDAO")
public class MailDAOImpl extends GenericDAOImpl<Mail> implements MailDAO {

}
