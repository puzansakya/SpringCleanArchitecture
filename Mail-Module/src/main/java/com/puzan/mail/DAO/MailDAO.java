/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.mail.DAO;

import com.puzan.core.DAO.GenericDAO;
import com.puzan.entity.Entities.Mail;

/**
 *
 * @author puzan
 */
public interface MailDAO extends GenericDAO<Mail>{
    
}
