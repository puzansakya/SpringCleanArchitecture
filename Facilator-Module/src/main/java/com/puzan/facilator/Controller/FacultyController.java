/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.facilator.Controller;

import com.puzan.entity.Entities.Faculties;
import com.puzan.facilator.DTO.FacultyDTO;
import com.puzan.facilator.Service.FacultyService;
import com.sun.media.sound.ModelMappedInstrument;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzan
 */
@Controller
@CrossOrigin
@RequestMapping(value = "admin/faculty")
public class FacultyController {

    @Autowired
    private FacultyService fs;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("active", "faculty");
        return "faculty/index";
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<FacultyDTO> getAll() {
        List<FacultyDTO> facultyDTOList = new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (Faculties faculty : fs.getAll()) {
            facultyDTOList.add(mapper.map(faculty, FacultyDTO.class));
        }
        return facultyDTOList;
    }

    @RequestMapping(value = "/search/{keyword}", method = RequestMethod.GET)
    @ResponseBody
    public List<FacultyDTO> search(@PathVariable("keyword") String keyword) {
        List<FacultyDTO> facultyDTOList = new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (Faculties faculty : fs.search(keyword)) {
            facultyDTOList.add(mapper.map(faculty, FacultyDTO.class));
        }
        return facultyDTOList;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public FacultyDTO save(@RequestBody FacultyDTO facultyDTO) {
        ModelMapper mapper = new ModelMapper();
        Faculties faculty = mapper.map(facultyDTO, Faculties.class);
        faculty.setDate(new Date());
        Faculties retFaculty = fs.insert(faculty);
        return mapper.map(retFaculty, FacultyDTO.class);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public FacultyDTO getById(@PathVariable("id") int id) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(fs.getById(id), FacultyDTO.class);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestBody FacultyDTO f) {
        ModelMapper mapper = new ModelMapper();
        fs.update(mapper.map(f, Faculties.class));
        return "[{\"response\":\"success\"}]";

    }

}
