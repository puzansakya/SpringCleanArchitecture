/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.facilator.DAO;


import com.puzan.core.DAO.GenericDAO;
import com.puzan.entity.Entities.Faculties;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface FacultyDAO extends GenericDAO<Faculties> {

    List<Faculties> search(String query);

}
