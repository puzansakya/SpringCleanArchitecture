/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.facilator.DAO.Impl;


import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.entity.Entities.Faculties;
import com.puzan.facilator.DAO.FacultyDAO;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "FacultyDAO")
public class FacultyDAOImpl extends GenericDAOImpl<Faculties> implements FacultyDAO {

    @Override
    public List<Faculties> search(String q) {
        session = sessionFactory.openSession();
        String hql = "from Faculties where firstname LIKE :searchKeyword";
        Query query = session.createQuery(hql);
        query.setParameter("searchKeyword", "%" + q + "%");
        List<Faculties> facultyList = query.list();
        session.close();
        return facultyList;
    }

}
