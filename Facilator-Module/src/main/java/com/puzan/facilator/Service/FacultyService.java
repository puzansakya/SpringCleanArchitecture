/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.facilator.Service;


import com.puzan.core.Service.GenericService;
import com.puzan.entity.Entities.Faculties;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface FacultyService extends GenericService<Faculties> {

    List<Faculties> search(String query);
}
