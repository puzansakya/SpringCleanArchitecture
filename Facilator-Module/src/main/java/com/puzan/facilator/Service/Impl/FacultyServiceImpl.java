/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.facilator.Service.Impl;


import com.puzan.entity.Entities.Faculties;
import com.puzan.facilator.DAO.FacultyDAO;
import com.puzan.facilator.Service.FacultyService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "FacultyService")
public class FacultyServiceImpl implements FacultyService {

    @Autowired
    private FacultyDAO fd;

    @Override
    public List<Faculties> getAll() {
        return fd.getAll();
    }

    @Override
    public Faculties insert(Faculties t) {
        return fd.insert(t);
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Faculties t) {
        fd.update(t);
    }

    @Override
    public Faculties getById(int id) {
        return fd.getById(id);
    }

    @Override
    public List<Faculties> search(String query) {
        return fd.search(query);
    }

}
