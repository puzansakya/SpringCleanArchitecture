package com.puzan.main.users.dao;

import com.puzan.entity.Entities.User;




public interface UserDao {

	User findByUserName(String username);

}