<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="http://localhost:8080/Main-Module/static/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="http://localhost:8080/Main-Module/static/assets/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="http://localhost:8080/Main-Module/static/assets/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="http://localhost:8080/Main-Module/static/assets/dist/css/AdminLTE.min.css">
        <!-- custom style -->
        <link rel="stylesheet" href="http://localhost:8080/Main-Module/static/assets/dist/css/custom.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="http://localhost:8080/Main-Module/static/assets/plugins/iCheck/square/blue.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">   
        <style>

            .header {
                height: 100%;
                /*                background: url('../img/background.jpg') bottom no-repeat transparent;*/
                background-size: cover;
            }

            .header .header__meta {
                position: absolute;
                top: 40px;
                right: 66px;
                font-size: 14px;
                z-index: 10;
            }

            .header .header__meta li {
                display: inline-block;
            }

            .header .header__meta li:after {
                display: inline-block;
                content: "";
                width: 1px;
                height: 10px;
                margin: 0 13px 0 17px;
                background-color: #fff;
                opacity: 0.46;
            }

            .header .header__meta li:last-of-type:after {
                display: none;
                visibility: hidden;
            }

            .header .header__meta strong {
                font-size: 13px;
                font-weight: bold;
            }

            .header .container {
                z-index: 10;
            }

            .header .header__introduction {
                padding-top: 60%;
            }

            .header .header__introduction p {
                opacity: 0.9;
            }

            .header .header__introduction .buttons {
                margin-top: 61px;
            }

            .header .header__introduction .buttons .button:first-of-type {
                margin-right: 42px;
            }

            .header .header__scroll {
                position: absolute;
                width: 110px;
                right: 0;
                left: 0;
                bottom: 29px;
                margin: auto;
                text-align: center;
                z-index: 10;
            }

            .header .header__background {
                position: absolute;
                display: block;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                z-index: 0;
            }


            .container {
                position: relative;
                max-width: 379px;
                margin: 0 auto;
                padding: 0 15px;
/*                text-align: center;*/
            }

            .container.container--large {
                max-width: 525px;
            }

            .container.container--large p {
                max-width: 377px;
                margin: 0 auto;
                text-align: center;
                line-height: 1.9;
            }


        </style>
    </head>
    <body class="hold-transition login-page header">

        <div class="container">
            <div class="login-box">
                <div class="login-logo">
                    <a href="#"><b>Admin</b>LTE</a>
                </div>
                <!-- /.login-logo -->
                <div class="login-box-body">
                    <p class="login-box-msg">Sign in to start your session</p>

                    <form  action="<c:url value='/login' />" method='POST'>
                        <div class="form-group has-feedback">
                            <input type="text" name="username" class="form-control" placeholder="Username">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" name='password' placeholder="Password">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="row">
                            <div class="col-xs-8">
                                <div class="checkbox icheck">
                                    <label>
                                        <input type="checkbox"> Remember Me
                                    </label>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                            </div>
                            <!-- /.col -->
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}"
                               value="${_csrf.token}" />
                    </form>

                    <div class="social-auth-links text-center">
                        <p>- OR -</p>
                        <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
                            Facebook</a>
                        <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
                            Google+</a>
                    </div>
                    <!-- /.social-auth-links -->

                    <a href="#">I forgot my password</a><br>
                    <a href="register.html" class="text-center">Register a new membership</a>

                </div>
                <!-- /.login-box-body -->
            </div>
            <!-- /.login-box -->
        </div>

        <!-- particle js  -->
        <canvas class="header__background"></canvas>

        <!-- jQuery 3 -->
        <script src="http://localhost:8080/Main-Module/static/assets/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="http://localhost:8080/Main-Module/static/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script src="http://localhost:8080/Main-Module/static/assets/plugins/iCheck/icheck.min.js"></script>
        <!-- particle js-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/particlesjs/2.1.0/particles.min.js "></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });

                Particles.init({
                    selector: '.header__background',
                    color: '#75A5B7',
                    maxParticles: 130,
                    connectParticles: false,
                    responsive: [
                        {
                            breakpoint: 768,
                            options: {
                                maxParticles: 80
                            }
                        }, {
                            breakpoint: 375,
                            options: {
                                maxParticles: 50
                            }
                        }
                    ]
                });

            });
        </script>
    </body>
</html>
