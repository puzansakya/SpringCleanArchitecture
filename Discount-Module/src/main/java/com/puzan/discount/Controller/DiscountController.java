/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.discount.Controller;

import com.puzan.discount.DTO.DiscountDTO;
import com.puzan.discount.Service.DiscountService;
import com.puzan.entity.Entities.Discount;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzansakya
 */
@Controller
@CrossOrigin
@RequestMapping(value = "/admin/discount")
public class DiscountController {

    @Autowired
    private DiscountService ds;

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "Discount:index";
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<DiscountDTO> getAll() {
        List<DiscountDTO> dDTOList = new ArrayList<>();
        for (Discount discount : ds.getAll()) {
            DiscountDTO d = new DiscountDTO();
            d.setDiscountId(discount.getDiscountId());
            d.setDiscount(discount.getDiscount());

            dDTOList.add(d);
        }

        return dDTOList;
    }
}
