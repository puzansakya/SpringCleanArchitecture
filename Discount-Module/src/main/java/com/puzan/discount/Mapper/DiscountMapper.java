/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.discount.Mapper;

import com.puzan.discount.DTO.DiscountDTO;
import com.puzan.entity.Entities.Discount;


/**
 *
 * @author puzan
 */
public class DiscountMapper {

    //convert entity to dto
    public DiscountDTO entityToDto(Discount d) {
        //create new DiscountDTO
        DiscountDTO dDto = new DiscountDTO();
        dDto.setDiscountId(d.getDiscountId());
        dDto.setDiscount(d.getDiscount());

        return dDto;

    }

    //convert dto to entity
    public Discount DtoToEntity(DiscountDTO dDTO) {
        Discount d = new Discount();
        d.setDiscountId(dDTO.getDiscountId());
        d.setDiscount(dDTO.getDiscount());

        return d;
    }
}
