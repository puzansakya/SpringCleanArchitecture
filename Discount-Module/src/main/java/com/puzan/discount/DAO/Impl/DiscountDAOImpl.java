/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.discount.DAO.Impl;


import com.puzan.discount.DAO.DiscountDAO;
import com.puzan.entity.Entities.Discount;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "DiscountDAO")
public class DiscountDAOImpl implements DiscountDAO {

    @Autowired
    private SessionFactory sf;
    private Session session;
    private Transaction transaction;

    @Override
    public List<Discount> getAll() {
        session = sf.openSession();
        List<Discount> discountList = session.createQuery("from Discount").list();
        session.close();
        return discountList;
    }

    @Override
    public Discount insert(Discount t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Discount t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Discount getById(int id) {
        session = sf.openSession();
        Discount course = (Discount) session.get(Discount.class, id);
        session.close();
        return course;
    }

}
