/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.discount.DAO;

import com.puzan.core.DAO.GenericDAO;
import com.puzan.entity.Entities.Discount;



/**
 *
 * @author puzan
 */
public interface DiscountDAO extends GenericDAO<Discount>{
    
}
