/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.discount.Service.Impl;

import com.puzan.discount.DAO.DiscountDAO;
import com.puzan.discount.Service.DiscountService;
import com.puzan.entity.Entities.Discount;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "DiscountService")
public class DiscountServiceImpl implements DiscountService {

    @Autowired
    private DiscountDAO dd;

    @Override
    public List<Discount> getAll() {
        return dd.getAll();
    }

    @Override
    public Discount insert(Discount t) {
        return dd.insert(t);
    }

    @Override
    public void delete(int id) {
       dd.delete(id);
    }

    @Override
    public void update(Discount t) {
        dd.update(t);
    }

    @Override
    public Discount getById(int id) {
       return dd.getById(id);
    }

}
