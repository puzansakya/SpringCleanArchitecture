/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.discount.Service;

import com.puzan.core.Service.GenericService;
import com.puzan.entity.Entities.Discount;


/**
 *
 * @author puzan
 */
public interface DiscountService extends GenericService<Discount>{
    
}
