/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.payment.DAO.Impl;

import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.entity.Entities.Payment;
import com.puzan.payment.DAO.PaymentDAO;
import com.puzan.payment.DTO.PaymentStatDTO;
import java.util.ArrayList;
import org.hibernate.SQLQuery;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "PaymentDAO")
public class PaymentDAOImpl extends GenericDAOImpl<Payment> implements PaymentDAO {

    @Override
    public List<Payment> search(String keyword) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PaymentStatDTO> getPaymentStat() {
        session = sessionFactory.openSession();
        String sql = "SELECT CAST(payment_date AS DATE) as 'Date',sum(amount) as 'amount' FROM `tbl_payments` GROUP BY CAST(payment_date AS DATE)";
//        String sql = "SELECT CAST(payment_date AS DATE) as 'Date',sum(amount) as 'amount' FROM `tbl_payments` WHERE payment_date BETWEEN'2017-09-14' AND  '2017-09-24' GROUP BY CAST(payment_date AS DATE)";
        SQLQuery query = session.createSQLQuery(sql);
        List<PaymentStatDTO> paymentStatDTOList = new ArrayList<>();
        List<Object[]> recs = query.list();
        for (Object[] line : recs) {
            PaymentStatDTO psDTO = new PaymentStatDTO();
            psDTO.setPaymentDate((line[0].toString()));
            psDTO.setAmount(Float.parseFloat(line[1].toString()));
            paymentStatDTOList.add(psDTO);
        }
        session.close();
        return paymentStatDTOList;
    }

}
