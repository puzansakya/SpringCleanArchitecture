/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.payment.Service.Impl;

import com.puzan.entity.Entities.Payment;
import com.puzan.payment.DAO.PaymentDAO;
import com.puzan.payment.DTO.PaymentStatDTO;
import com.puzan.payment.Service.PaymentService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "PaymentService")
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentDAO pd;

    @Override
    public List<Payment> search(String keyword) {
        return pd.search(keyword);
    }

    @Override
    public List<Payment> getAll() {
        return pd.getAll();
    }

    @Override
    public Payment insert(Payment t) {
        return pd.insert(t);
    }

    @Override
    public void delete(int id) {
        pd.delete(id);
    }

    @Override
    public void update(Payment t) {
        pd.update(t);
    }

    @Override
    public Payment getById(int id) {
        return pd.getById(id);
    }

    @Override
    public List<PaymentStatDTO> getPaymentStat() {
        return pd.getPaymentStat();
    }

}
