/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.payment.Service;

import com.puzan.core.Service.GenericService;
import com.puzan.entity.Entities.Payment;
import com.puzan.payment.DTO.PaymentStatDTO;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface PaymentService extends GenericService<Payment> {

    List<Payment> search(String keyword);

    List<PaymentStatDTO> getPaymentStat();
}
