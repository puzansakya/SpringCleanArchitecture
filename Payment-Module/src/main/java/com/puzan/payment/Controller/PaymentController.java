/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.payment.Controller;

import com.puzan.enrollment.DTO.EnrollmentDTO;
import com.puzan.enrollment.Service.EnrollmentService;
import com.puzan.entity.Entities.Payment;
import com.puzan.payment.DTO.PaymentDTO;
import com.puzan.payment.Service.PaymentService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzan
 */
@Controller
@CrossOrigin
@RequestMapping(value = "admin/payment")
public class PaymentController {

    @Autowired
    private PaymentService ps;

    @Autowired
    private EnrollmentService es;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String index() {
        return "payment : index";
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<PaymentDTO> getAll() {
        ModelMapper mapper = new ModelMapper();
        List<PaymentDTO> pDTOList = new ArrayList<>();
        for (Payment payment : ps.getAll()) {
            mapper.map(payment.getEnrollmentId(), EnrollmentDTO.class);
            pDTOList.add(mapper.map(payment, PaymentDTO.class));
        }
        return pDTOList;
    }

    @RequestMapping(value = "/search/{keyword}", method = RequestMethod.GET)
    @ResponseBody
    public List<PaymentDTO> search(@PathVariable("keyword") String keyword) {
        return null;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public String save(@RequestBody PaymentDTO pDTO) {
//        ModelMapper mapper = new ModelMapper();
//        Payment payment = mapper.map(pDTO, Payment.class);
//        payment.setEnrollmentId(es.getById(pDTO.getEnrollment().getEnrollmentId()));
//        Payment retPayment = ps.insert(payment);
//        mapper.map(retPayment.getEnrollmentId(), EnrollmentDTO.class);
//        return mapper.map(retPayment, PaymentDTO.class);
        Payment payment = new Payment();
        payment.setAmount(pDTO.getAmount());
        payment.setPaymentDate(new Date());
        payment.setPaymentType(pDTO.isPaymentType());
        payment.setBank(pDTO.getBank());
        payment.setCheckNo(pDTO.getCheckNo());
        payment.setReceiptNo(pDTO.getReceiptNo());

        payment.setEnrollmentId(es.getById(pDTO.getEnrollment().getEnrollmentId()));

        Payment retPayment = ps.insert(payment);
        if (retPayment != null) {
            return "success";
        }
        return "failed";

    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String update(PaymentDTO pDTO) {
        ModelMapper mapper = new ModelMapper();
        Payment payment = mapper.map(pDTO, Payment.class);
        payment.setEnrollmentId(es.getById(pDTO.getEnrollment().getEnrollmentId()));
        ps.update(payment);
        return "success";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String delete(@PathVariable("id") int id) {
        return null;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public PaymentDTO getById(@PathVariable("id") int id) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(ps.getById(id), PaymentDTO.class);
    }

}
