/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.payment.DTO;

import com.puzan.helper.DateHelper.DateHelper;

/**
 *
 * @author puzan
 */
public class PaymentStatDTO {

    private String paymentDate;
    private float amount;

    public String getPaymentDate() {
        return DateHelper.newInstance().formatDateMonthDay(paymentDate);
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

}
