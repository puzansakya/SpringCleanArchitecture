/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enquiry.DAO;

import com.puzan.core.DAO.GenericDAO;
import com.puzan.entity.Entities.Enquiry;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface EnquiryDAO extends GenericDAO<Enquiry> {

    List<Enquiry> search(String query);
    
    List<Enquiry> getAllPendingEnquiry();

    Long countPendingEnquiries();
    
    Long getNotInterestEnquiry();
    
    Long getAllEnquiryCount();
    
    
}
