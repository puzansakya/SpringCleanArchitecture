/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enquiry.DAO.Impl;

import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.enquiry.DAO.EnquiryDAO;
import com.puzan.entity.Entities.Enquiry;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "EnquiryDAO")
public class EnquiryDAOImpl extends GenericDAOImpl<Enquiry> implements EnquiryDAO {

    @Override
    public List<Enquiry> search(String Keyword) {
        session = sessionFactory.openSession();
        String hql = "from Enquiry where firstName LIKE :searchKeyword";
        Query query = session.createQuery(hql);
        query.setParameter("searchKeyword", "%" + Keyword + "%");
        List<Enquiry> enquiryList = query.list();
        session.close();
        return enquiryList;
    }

    @Override
    public Long countPendingEnquiries() {
        session = sessionFactory.openSession();
        String hql = "SELECT count(1) FROM Enquiry where enroll_status_id = 1";
        Query query = session.createQuery(hql);
        Long count = (Long) query.uniqueResult();
        session.close();
        return count;
    }

    @Override
    public Long getNotInterestEnquiry() {
        session = sessionFactory.openSession();
        String hql = "SELECT count(1) FROM Enquiry where enroll_status_id = 3";
        Query query = session.createQuery(hql);
        Long count = (Long) query.uniqueResult();
        session.close();
        return count;
    }

    @Override
    public Long getAllEnquiryCount() {
        session = sessionFactory.openSession();
        String hql = "SELECT count(1) FROM Enquiry";
        Long count = (Long) session.createQuery(hql).uniqueResult();
        session.close();
        return count;
    }

    @Override
    public List<Enquiry> getAllPendingEnquiry() {
        session = sessionFactory.openSession();
        String hql = "from Enquiry where enrollStatusId.enrollId = 1 ";
        Query query = session.createQuery(hql);
        List<Enquiry> enquiryList = query.list();
        session.close();
        return enquiryList;
    }

}
