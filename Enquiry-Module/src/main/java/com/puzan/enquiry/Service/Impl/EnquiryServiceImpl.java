/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enquiry.Service.Impl;

import com.puzan.enquiry.DAO.EnquiryDAO;
import com.puzan.enquiry.Service.EnquiryService;
import com.puzan.entity.Entities.Enquiry;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "EnquiryService")
public class EnquiryServiceImpl implements EnquiryService {

    @Autowired
    private EnquiryDAO ed;

    @Override
    public List<Enquiry> getAll() {
        return ed.getAll();
    }

    @Override
    public Enquiry insert(Enquiry t) {
        return ed.insert(t);
    }

    @Override
    public void delete(int id) {
        ed.delete(id);
    }

    @Override
    public void update(Enquiry t) {
        ed.update(t);
    }

    @Override
    public Enquiry getById(int id) {
        return ed.getById(id);
    }

    @Override
    public List<Enquiry> search(String key) {
        return ed.search(key);
    }

    @Override
    public Long countPendingEnquiries() {
        return ed.countPendingEnquiries();
    }

    @Override
    public Long getNotInterestEnquiry() {
        return ed.getNotInterestEnquiry();
    }

    @Override
    public Long getAllEnquiryCount() {
        return ed.getAllEnquiryCount();
    }

    @Override
    public List<Enquiry> getAllPendingEnquiry() {
        return ed.getAllPendingEnquiry();
    }

}
