/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enquiry.Service;

import com.puzan.core.Service.GenericService;
import com.puzan.entity.Entities.Enquiry;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface EnquiryService extends GenericService<Enquiry> {

    List<Enquiry> search(String key);

    Long countPendingEnquiries();

    Long getNotInterestEnquiry();

    Long getAllEnquiryCount();

    List<Enquiry> getAllPendingEnquiry();
}
