/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enquiry.DTO;

import com.puzan.course.DTO.CourseDTO;
import com.puzan.enrollstatus.DTO.EnrollStatusDTO;
import java.util.Date;

/**
 *
 * @author puzan
 */
public class EnquiryDTO {

    private Integer enquiryId;
    private String firstname;
    private String lastname;
    private String email;
    private int contactNo;
    private Date enquiryDate;
    private String message;
    private CourseDTO course;
    private EnrollStatusDTO enrollStatus;
    private int follow_up_count;

    public Integer getEnquiryId() {
        return enquiryId;
    }

    public void setEnquiryId(Integer enquiryId) {
        this.enquiryId = enquiryId;
    }

    public int getFollow_up_count() {
        return follow_up_count;
    }

    public void setFollow_up_count(int follow_up_count) {
        this.follow_up_count = follow_up_count;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getContactNo() {
        return contactNo;
    }

    public void setContactNo(int contactNo) {
        this.contactNo = contactNo;
    }

    public Date getEnquiryDate() {
        return enquiryDate;
    }

    public void setEnquiryDate(Date enquiryDate) {
        this.enquiryDate = enquiryDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CourseDTO getCourse() {
        return course;
    }

    public void setCourse(CourseDTO course) {
        this.course = course;
    }

  

    public EnrollStatusDTO getEnrollStatus() {
        return enrollStatus;
    }

    public void setEnrollStatus(EnrollStatusDTO enrollStatus) {
        this.enrollStatus = enrollStatus;
    }

  

  

}
