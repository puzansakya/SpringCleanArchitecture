/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enquiry.Controller;

import com.puzan.batch.Service.BatchService;
import com.puzan.course.DTO.CourseDTO;
import com.puzan.course.Service.CourseService;
import com.puzan.discount.Service.DiscountService;
import com.puzan.enquiry.DTO.EnquiryDTO;
import com.puzan.enquiry.Service.EnquiryService;
import com.puzan.enrollstatus.DTO.EnrollStatusDTO;
import com.puzan.enrollstatus.Service.EnrollStatusService;
import com.puzan.entity.Entities.Course;
import com.puzan.entity.Entities.Enquiry;
import com.puzan.entity.Entities.EnrollStatus;
import com.puzan.followup.Service.FollowUpService;
import com.puzan.time.Service.TimeService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzan
 */
@Controller
@CrossOrigin(maxAge = 3600)
@RequestMapping(value = "admin/enquiry")
public class EnquiryController {

    @Autowired
    private EnquiryService es;

    @Autowired
    private CourseService cs;

    @Autowired
    private TimeService ts;

    @Autowired
    private DiscountService ds;

    @Autowired
    private BatchService bs;

    @Autowired
    private EnrollStatusService ess;

    @Autowired
    private FollowUpService fs;

    @RequestMapping(method = RequestMethod.GET)
    private String index(Model model) {
        model.addAttribute("courseList", cs.getAll());
        model.addAttribute("timeList", ts.getAll());
        model.addAttribute("discountList", ds.getAll());
        model.addAttribute("batchList", bs.getAll());
        model.addAttribute("enrollStatusList", ess.getAll());
        model.addAttribute("active", "enquiry");
        return "enquiry/index";
    }

    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    @ResponseBody
    public List<EnquiryDTO> getAll() {
        ModelMapper mapper = new ModelMapper();
         List<EnquiryDTO> enquiryDTOList = new ArrayList<>();
        for (Enquiry e : es.getAll()) {
            mapper.map(e.getCourseId(), CourseDTO.class);
            mapper.map(e.getEnrollStatusId(), EnrollStatusDTO.class);
            EnquiryDTO retEnquiryDTO = mapper.map(e, EnquiryDTO.class);
            retEnquiryDTO.setFollow_up_count(fs.countFollowUp(e.getEnquiryId()).intValue());
            enquiryDTOList.add(retEnquiryDTO);
        }
        return enquiryDTOList;
    }

    @RequestMapping(value = "/getAllPending", method = RequestMethod.GET)
    @ResponseBody
    public List<EnquiryDTO> getAllPending() {
        ModelMapper mapper = new ModelMapper();
        List<EnquiryDTO> enquiryDTOList = new ArrayList<>();
        for (Enquiry e : es.getAllPendingEnquiry()) {
            mapper.map(e.getCourseId(), CourseDTO.class);
            mapper.map(e.getEnrollStatusId(), EnrollStatusDTO.class);
            EnquiryDTO retEnquiryDTO = mapper.map(e, EnquiryDTO.class);
            retEnquiryDTO.setFollow_up_count(fs.countFollowUp(e.getEnquiryId()).intValue());
            enquiryDTOList.add(retEnquiryDTO);
        }
        return enquiryDTOList;
    }

    @RequestMapping(value = "/search/{keyword}", method = RequestMethod.GET)
    @ResponseBody
    public List<EnquiryDTO> search(@PathVariable("keyword") String keyword) {
        ModelMapper mapper = new ModelMapper();
        List<EnquiryDTO> enquiryDTOList = new ArrayList<>();
        for (Enquiry e : es.search(keyword)) {
            mapper.map(e.getCourseId(), CourseDTO.class);
            mapper.map(e.getEnrollStatusId(), EnrollStatusDTO.class);
            EnquiryDTO retEnquiryDTO = mapper.map(e, EnquiryDTO.class);
            retEnquiryDTO.setFollow_up_count(fs.countFollowUp(e.getEnquiryId()).intValue());
            enquiryDTOList.add(retEnquiryDTO);
        }
        return enquiryDTOList;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    private EnquiryDTO save(@RequestBody EnquiryDTO enquiryDTO) {
        ModelMapper mapper = new ModelMapper();
        Course c = cs.getById(enquiryDTO.getCourse().getCourseId());
        EnrollStatus enrollStatus = ess.getById(enquiryDTO.getEnrollStatus().getEnrollId());

        mapper.map(c, CourseDTO.class);
        mapper.map(enrollStatus, EnrollStatusDTO.class);

        Enquiry enquiry = mapper.map(enquiryDTO, Enquiry.class);
        enquiry.setEnquiryDate(new Date());

        Enquiry retEnquiry = es.insert(enquiry);

        retEnquiry.setCourseId(c);
        retEnquiry.setEnrollStatusId(enrollStatus);
        return mapper.map(retEnquiry, EnquiryDTO.class);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    private String update(@RequestBody EnquiryDTO enquiryDTO) {
        ModelMapper mapper = new ModelMapper();
        Course c = cs.getById(enquiryDTO.getCourse().getCourseId());
        EnrollStatus enrollStatus = ess.getById(enquiryDTO.getEnrollStatus().getEnrollId());

        mapper.map(c, CourseDTO.class);
        mapper.map(enrollStatus, EnrollStatusDTO.class);

        Enquiry enquiry = mapper.map(enquiryDTO, Enquiry.class);
        enquiry.setEnquiryDate(new Date());
        es.update(mapper.map(enquiryDTO, Enquiry.class));
        return "success";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public EnquiryDTO getById(@PathVariable("id") int id) {
        ModelMapper mapper = new ModelMapper();
        Enquiry e = es.getById(id);
        mapper.map(e.getCourseId(), Course.class);
        mapper.map(e.getEnrollStatusId(), EnrollStatus.class);
        return mapper.map(e, EnquiryDTO.class);
    }

    @RequestMapping(value = "/getPendingEnquiries", method = RequestMethod.GET)
    public @ResponseBody
    String getPendingEnquiries() {
        return String.valueOf(es.countPendingEnquiries());
    }
}
