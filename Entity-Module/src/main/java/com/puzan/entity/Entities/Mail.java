/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.entity.Entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author puzan
 */
@Entity
@Table(name = "tbl_mails")
@NamedQueries({
    @NamedQuery(name = "Mail.findAll", query = "SELECT m FROM Mail m")})
public class Mail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)   
    @Column(name = "mail_id")
    private Integer mailId;
    
    @Column(name = "subject")
    private String subject;
    
    @Column(name = "message")
    private String message;
    
    @Column(name = "send_to")
    private String sendTo;
    
    @Column(name = "send_date",insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date sendDate;
    
    @Column(name = "isDelete")
    private boolean isDelete;
    
    @JoinColumn(name = "send_by", referencedColumnName = "username")
    @ManyToOne(optional = false)
    private User sendBy;

    public Mail() {
    }

    public Mail(Integer mailId) {
        this.mailId = mailId;
    }

    public Mail(Integer mailId, String subject, String message, String sendTo, Date sendDate, boolean isDelete) {
        this.mailId = mailId;
        this.subject = subject;
        this.message = message;
        this.sendTo = sendTo;
        this.sendDate = sendDate;
        this.isDelete = isDelete;
    }

    public Integer getMailId() {
        return mailId;
    }

    public void setMailId(Integer mailId) {
        this.mailId = mailId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSendTo() {
        return sendTo;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public User getSendBy() {
        return sendBy;
    }

    public void setSendBy(User sendBy) {
        this.sendBy = sendBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mailId != null ? mailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mail)) {
            return false;
        }
        Mail other = (Mail) object;
        if ((this.mailId == null && other.mailId != null) || (this.mailId != null && !this.mailId.equals(other.mailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.puzan.shmm.entity.Entities.Mail[ mailId=" + mailId + " ]";
    }
    
}
