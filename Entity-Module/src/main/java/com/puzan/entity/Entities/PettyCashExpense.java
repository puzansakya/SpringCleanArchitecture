/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.entity.Entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author puzan
 */
@Entity
@Table(name = "tbl_petty_cash_expenses")
@NamedQueries({
    @NamedQuery(name = "PettyCashExpense.findAll", query = "SELECT p FROM PettyCashExpense p")})
public class PettyCashExpense implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)    
    @Column(name = "pt_exp_id")
    private Integer ptExpId;
    
    @Column(name = "expDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expDate;
    
    @Column(name = "amount")
    private double amount;
    
    @JoinColumn(name = "expBy", referencedColumnName = "username")
    @ManyToOne(optional = false)
    private User expBy;

    public PettyCashExpense() {
    }

    public PettyCashExpense(Integer ptExpId) {
        this.ptExpId = ptExpId;
    }

    public PettyCashExpense(Integer ptExpId, Date expDate, double amount) {
        this.ptExpId = ptExpId;
        this.expDate = expDate;
        this.amount = amount;
    }

    public Integer getPtExpId() {
        return ptExpId;
    }

    public void setPtExpId(Integer ptExpId) {
        this.ptExpId = ptExpId;
    }

    public Date getExpDate() {
        return expDate;
    }

    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public User getExpBy() {
        return expBy;
    }

    public void setExpBy(User expBy) {
        this.expBy = expBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ptExpId != null ? ptExpId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PettyCashExpense)) {
            return false;
        }
        PettyCashExpense other = (PettyCashExpense) object;
        if ((this.ptExpId == null && other.ptExpId != null) || (this.ptExpId != null && !this.ptExpId.equals(other.ptExpId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.puzan.shmm.entity.Entities.PettyCashExpense[ ptExpId=" + ptExpId + " ]";
    }
    
}
