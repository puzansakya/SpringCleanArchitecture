/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.entity.Entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author puzan
 */
@Entity
@Table(name = "tbl_pettycash_allocations")
@NamedQueries({
    @NamedQuery(name = "PettycashAllocation.findAll", query = "SELECT p FROM PettycashAllocation p")})
public class PettycashAllocation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pt_allocation_id")
    private Integer ptAllocationId;

    @Column(name = "amount")
    private double amount;

    @Column(name = "releasedDate", insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date releasedDate;

    @JoinColumn(name = "releasedBy", referencedColumnName = "username")
    @ManyToOne(optional = false)
    private User releasedBy;

    public PettycashAllocation() {
    }

    public PettycashAllocation(Integer ptAllocationId) {
        this.ptAllocationId = ptAllocationId;
    }

    public PettycashAllocation(Integer ptAllocationId, double amount, Date releasedDate) {
        this.ptAllocationId = ptAllocationId;
        this.amount = amount;
        this.releasedDate = releasedDate;
    }

    public Integer getPtAllocationId() {
        return ptAllocationId;
    }

    public void setPtAllocationId(Integer ptAllocationId) {
        this.ptAllocationId = ptAllocationId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getReleasedDate() {
        return releasedDate;
    }

    public void setReleasedDate(Date releasedDate) {
        this.releasedDate = releasedDate;
    }

    public User getReleasedBy() {
        return releasedBy;
    }

    public void setReleasedBy(User releasedBy) {
        this.releasedBy = releasedBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ptAllocationId != null ? ptAllocationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PettycashAllocation)) {
            return false;
        }
        PettycashAllocation other = (PettycashAllocation) object;
        if ((this.ptAllocationId == null && other.ptAllocationId != null) || (this.ptAllocationId != null && !this.ptAllocationId.equals(other.ptAllocationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.puzan.shmm.entity.Entities.PettycashAllocation[ ptAllocationId=" + ptAllocationId + " ]";
    }

}
