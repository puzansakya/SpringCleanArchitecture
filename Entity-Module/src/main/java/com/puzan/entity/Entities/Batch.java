/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.entity.Entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author puzan
 */
@Entity
@Table(name = "tbl_batch")
public class Batch implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "batchId")
    private List<Enrollment> enrollmentList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "batch_id")
    private Integer batchId;

    @Column(name = "created_date", insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @Column(name = "start_date", insertable = false, nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    
    @Column(name = "end_date", nullable = false, insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @Column(name = "status")
    private boolean status;

    @Column(name = "payment_status")
    private boolean paymentStatus;

    @Column(name = "agreement",nullable = true)
    private String agreement;

    @Transient
    private MultipartFile file;

    @JoinColumn(name = "batch_code_id", referencedColumnName = "batch_code_id")
    @ManyToOne(cascade = CascadeType.ALL)
    private BatchCodeGenerator batchCodeId;

    @JoinColumn(name = "faculty_id", referencedColumnName = "faculty_id")
    @ManyToOne(optional = false)
    private Faculties facultyId;

    @JoinColumn(name = "course_id", referencedColumnName = "course_id")
    @ManyToOne(optional = false)
    private Course courseId;

    @JoinColumn(name = "time_id", referencedColumnName = "time_id")
    @ManyToOne(optional = false)
    private Time timeId;

    public Batch() {
    }

    public Batch(Integer batchId) {
        this.batchId = batchId;
    }

    public Batch(Integer batchId, Date createdDate, boolean status, boolean paymentStatus, String agreement) {
        this.batchId = batchId;
        this.createdDate = createdDate;
        this.status = status;
        this.paymentStatus = paymentStatus;
        this.agreement = agreement;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public Integer getBatchId() {
        return batchId;
    }

    public void setBatchId(Integer batchId) {
        this.batchId = batchId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getAgreement() {
        return agreement;
    }

    public void setAgreement(String agreement) {
        this.agreement = agreement;
    }

    public BatchCodeGenerator getBatchCodeId() {
        return batchCodeId;
    }

    public void setBatchCodeId(BatchCodeGenerator batchCodeId) {
        this.batchCodeId = batchCodeId;
    }

    public Faculties getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Faculties facultyId) {
        this.facultyId = facultyId;
    }

    public Course getCourseId() {
        return courseId;
    }

    public void setCourseId(Course courseId) {
        this.courseId = courseId;
    }

    public Time getTimeId() {
        return timeId;
    }

    public void setTimeId(Time timeId) {
        this.timeId = timeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (batchId != null ? batchId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Batch)) {
            return false;
        }
        Batch other = (Batch) object;
        if ((this.batchId == null && other.batchId != null) || (this.batchId != null && !this.batchId.equals(other.batchId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.puzan.shmm.entity.Entities.Batch[ batchId=" + batchId + " ]";
    }

    public List<Enrollment> getEnrollmentList() {
        return enrollmentList;
    }

    public void setEnrollmentList(List<Enrollment> enrollmentList) {
        this.enrollmentList = enrollmentList;
    }

}
