/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.bank.Service.Impl;


import com.puzan.bank.DAO.BankVoucherDAO;
import com.puzan.bank.Service.BankVoucherService;
import com.puzan.entity.Entities.BankVoucher;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "BankVoucherService")
public class BankVoucherServiceImpl implements BankVoucherService {
    @Autowired
    private BankVoucherDAO bvd;

    @Override
    public List<BankVoucher> getAll() {
       return bvd.getAll();
    }

    @Override
    public BankVoucher insert(BankVoucher t) {
       return bvd.insert(t);
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(BankVoucher t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BankVoucher getById(int id) {
       return bvd.getById(id);
    }

}
