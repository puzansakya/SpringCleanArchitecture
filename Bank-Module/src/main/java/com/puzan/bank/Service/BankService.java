/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.bank.Service;


import com.puzan.core.Service.GenericService;
import com.puzan.entity.Entities.Bank;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface BankService extends GenericService<Bank> {
    List<Bank> search(String keyword);
}
