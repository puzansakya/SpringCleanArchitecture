/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.bank.Service.Impl;


import com.puzan.bank.DAO.BankDAO;
import com.puzan.bank.Service.BankService;
import com.puzan.entity.Entities.Bank;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "BankService")
public class BankServiceImpl implements BankService {

    @Autowired
    private BankDAO bd;

    @Override
    public List<Bank> getAll() {
        return bd.getAll();
    }

    @Override
    public Bank insert(Bank t) {
        return bd.insert(t);
    }

    @Override
    public void delete(int id) {
        bd.delete(id);
    }

    @Override
    public void update(Bank t) {
        bd.update(t);
    }

    @Override
    public Bank getById(int id) {
        return bd.getById(id);
    }

    @Override
    public List<Bank> search(String keyword) {
        return bd.search(keyword);
    }

}
