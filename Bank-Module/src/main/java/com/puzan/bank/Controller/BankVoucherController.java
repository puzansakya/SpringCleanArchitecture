/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.bank.Controller;


import com.puzan.bank.Service.BankService;
import com.puzan.bank.Service.BankVoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzan
 */
@Controller
@CrossOrigin
@RequestMapping(value = "/admin/bankVoucher")
public class BankVoucherController {

    @Autowired
    private BankVoucherService bvs;

    @Autowired
    private BankService bs;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("bankVoucherList", bvs.getAll());
        model.addAttribute("bankList", bs.getAll());
        model.addAttribute("active", "expense");
        model.addAttribute("subactive", "bankVoucher");
        return "bank/bankVoucher";
    }

}
