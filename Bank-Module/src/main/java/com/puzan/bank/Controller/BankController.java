/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.bank.Controller;

import com.puzan.bank.DTO.BankDTO;
import com.puzan.bank.Service.BankService;
import com.puzan.entity.Entities.Bank;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzan
 */
@Controller
@CrossOrigin
@RequestMapping(value = "/admin/bank")
public class BankController {

    @Autowired
    private BankService bs;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("active", "expense");
        model.addAttribute("subactive", "bank");

        return "bank/index";
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<BankDTO> getAll() {
        ModelMapper mapper = new ModelMapper();
        List<BankDTO> bankDTOList = new ArrayList<>();
        for (Bank bank : bs.getAll()) {
            bankDTOList.add(mapper.map(bank, BankDTO.class));
        }
        return bankDTOList;
    }

    @RequestMapping(value = "/search/{keyword}", method = RequestMethod.GET)
    @ResponseBody
    public List<BankDTO> search(@PathVariable("keyword") String keyword) {
        ModelMapper mapper = new ModelMapper();
        List<BankDTO> bankDTOList = new ArrayList<>();
        for (Bank bank : bs.search(keyword)) {
            bankDTOList.add(mapper.map(bank, BankDTO.class));
        }
        return bankDTOList;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public BankDTO save(@RequestBody BankDTO bDTO) {
        ModelMapper mapper = new ModelMapper();
        Bank bank = mapper.map(bDTO, Bank.class);
        bank.setCreatedDate(new Date());
        Bank retBank = bs.insert(bank);
        return mapper.map(retBank, BankDTO.class);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestBody BankDTO bDTO) {
        ModelMapper mapper = new ModelMapper();
        bs.update(mapper.map(bDTO, Bank.class));
        return "[{\"response\":\"success\"}]";

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public BankDTO getById(@PathVariable("id") int id) {
        ModelMapper mapper = new ModelMapper();
        Bank bank = bs.getById(id);
        return mapper.map(bank, BankDTO.class);
    }

}
