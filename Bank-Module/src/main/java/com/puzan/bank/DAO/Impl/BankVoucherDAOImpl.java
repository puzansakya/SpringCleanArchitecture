/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.bank.DAO.Impl;


import com.puzan.bank.DAO.BankVoucherDAO;
import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.entity.Entities.BankVoucher;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "BankVoucherDAO")
public class BankVoucherDAOImpl extends GenericDAOImpl<BankVoucher> implements BankVoucherDAO {

}
