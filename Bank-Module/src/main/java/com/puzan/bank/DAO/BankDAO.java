/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.bank.DAO;


import com.puzan.core.DAO.GenericDAO;
import com.puzan.entity.Entities.Bank;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface BankDAO extends GenericDAO<Bank> {

    List<Bank> search(String keyword);

}
