/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.bank.DAO.Impl;

import com.puzan.bank.DAO.BankDAO;
import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.entity.Entities.Bank;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "BankDAO")
public class BankDAOImpl extends GenericDAOImpl<Bank> implements BankDAO {

    @Override
    public List<Bank> search(String keyword) {
        session = sessionFactory.openSession();
        String hql = "from Bank where bankName LIKE :searchKeyword";
        Query query = session.createQuery(hql);
        query.setParameter("searchKeyword", "%" + keyword + "%");
        List<Bank> bankList = query.list();
        session.close();
        return bankList;
    }

}
