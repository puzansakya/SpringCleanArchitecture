/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.pettycash.DAO.Impl;

import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.entity.Entities.PettyCashExpense;
import com.puzan.pettycash.DAO.PCEDAO;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "PCEDAO")
public class PCEDAOImpl extends GenericDAOImpl<PettyCashExpense> implements PCEDAO {

}
