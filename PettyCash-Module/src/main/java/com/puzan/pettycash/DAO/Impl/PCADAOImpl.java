/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.pettycash.DAO.Impl;

import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.entity.Entities.Enrollment;
import com.puzan.entity.Entities.PettycashAllocation;
import com.puzan.pettycash.DAO.PCADAO;
import java.util.List;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "PCADAO")
public class PCADAOImpl extends GenericDAOImpl<PettycashAllocation> implements PCADAO {

    @Override
    public List<PettycashAllocation> search(String keyword) {
        session = sessionFactory.openSession();
        String hql = "from PettycashAllocation where releasedBy.username LIKE :searchKeyword";
        Query query = session.createQuery(hql);
        query.setParameter("searchKeyword", "%" + keyword + "%");
        List<PettycashAllocation> searchList = query.list();
        session.close();
        return searchList;
    }

}
