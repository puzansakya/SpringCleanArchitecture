/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.pettycash.Service;

import com.puzan.core.Service.GenericService;
import com.puzan.entity.Entities.PettycashAllocation;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface PCAService extends GenericService<PettycashAllocation> {

    List<PettycashAllocation> search(String keyword);
}
