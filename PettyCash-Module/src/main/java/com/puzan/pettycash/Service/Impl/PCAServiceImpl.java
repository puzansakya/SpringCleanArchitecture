/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.pettycash.Service.Impl;

import com.puzan.entity.Entities.PettycashAllocation;
import com.puzan.pettycash.DAO.PCADAO;
import com.puzan.pettycash.Service.PCAService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "PCAService")
public class PCAServiceImpl implements PCAService {

    @Autowired
    private PCADAO pcaDAO;

    @Override
    public List<PettycashAllocation> search(String keyword) {
        return pcaDAO.search(keyword);
    }

    @Override
    public List<PettycashAllocation> getAll() {
        return pcaDAO.getAll();
    }

    @Override
    public PettycashAllocation insert(PettycashAllocation t) {
        return pcaDAO.insert(t);
    }

    @Override
    public void delete(int id) {
        pcaDAO.delete(id);
    }

    @Override
    public void update(PettycashAllocation t) {
        pcaDAO.update(t);
    }

    @Override
    public PettycashAllocation getById(int id) {
        return pcaDAO.getById(id);
    }

}
