/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.pettycash.Service.Impl;

import com.puzan.entity.Entities.PettyCashExpense;
import com.puzan.pettycash.DAO.PCEDAO;
import com.puzan.pettycash.Service.PCEService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "PCEService")
public class PCEServiceImpl implements PCEService {

    @Autowired
    private PCEDAO pceDAO;

    @Override
    public List<PettyCashExpense> getAll() {
        return pceDAO.getAll();
    }

    @Override
    public PettyCashExpense insert(PettyCashExpense t) {
        return pceDAO.insert(t);
    }

    @Override
    public void delete(int id) {
        pceDAO.delete(id);
    }

    @Override
    public void update(PettyCashExpense t) {
        pceDAO.update(t);
    }

    @Override
    public PettyCashExpense getById(int id) {
        return pceDAO.getById(id);
    }

}
