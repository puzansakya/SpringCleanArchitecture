/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.pettycash.Controller;

import com.puzan.entity.Entities.PettyCashExpense;
import com.puzan.pettycash.DTO.PCADTO;
import com.puzan.pettycash.DTO.PCEDTO;
import com.puzan.pettycash.Service.PCEService;
import com.puzan.user.DTO.UserDTO;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzan
 */
@Controller
@CrossOrigin
@RequestMapping(value = "/admin/pce")
public class PCEController {

    @Autowired
    private PCEService pces;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("active", "expense");
        model.addAttribute("subactive", "pce");

        return "pettycash/PCE";
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<PCEDTO> getAll() {
        List<PCEDTO> pceDTOList = new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (PettyCashExpense pce : pces.getAll()) {
            mapper.map(pce.getExpBy(), UserDTO.class);
            pceDTOList.add(mapper.map(pce, PCEDTO.class));
        }
        return pceDTOList;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public PCEDTO save(@RequestBody PCEDTO pceDTO) {
        ModelMapper mapper = new ModelMapper();
        mapper.map(pceDTO.getExpBy(), UserDTO.class);
        PettyCashExpense pce = mapper.map(pceDTO, PettyCashExpense.class);
        pce.setExpDate(new Date());
        PettyCashExpense retPCE = pces.insert(pce);
        mapper.map(retPCE.getExpBy(), UserDTO.class);
        return mapper.map(retPCE, PCEDTO.class);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestBody PCEDTO pceDTO) {
        ModelMapper mapper = new ModelMapper();
        mapper.map(pceDTO.getExpBy(), UserDTO.class);
        PettyCashExpense pce = mapper.map(pceDTO, PettyCashExpense.class);
        pce.setExpDate(new Date());
        pces.update(pce);
        return "[{\"response\":\"sucess\"}]";
    }

}
