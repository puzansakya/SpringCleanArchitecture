/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.pettycash.Controller;

import com.puzan.entity.Entities.PettycashAllocation;
import com.puzan.pettycash.DTO.PCADTO;
import com.puzan.pettycash.Service.PCAService;
import com.puzan.user.DTO.UserDTO;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzan
 */
@Controller
@CrossOrigin
@RequestMapping(value = "/admin/pca")
public class PCAController {

    @Autowired
    private PCAService pcas;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("active", "expense");
        model.addAttribute("subactive", "pca");

        return "pettycash/PCA";
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<PCADTO> getAll() {
        List<PCADTO> pcaDTOList = new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (PettycashAllocation pca : pcas.getAll()) {
            mapper.map(pca.getReleasedBy(), UserDTO.class);
            pcaDTOList.add(mapper.map(pca, PCADTO.class));
        }
        return pcaDTOList;
    }

    @RequestMapping(value = "/search/{keyword}", method = RequestMethod.GET)
    @ResponseBody
    public List<PCADTO> search(@PathVariable("keyword") String keyword) {
        List<PCADTO> pcaDTOList = new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (PettycashAllocation pca : pcas.search(keyword)) {
            mapper.map(pca.getReleasedBy(), UserDTO.class);
            pcaDTOList.add(mapper.map(pca, PCADTO.class));
        }
        return pcaDTOList;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public PCADTO save(@RequestBody PCADTO pcaDTO) {
        ModelMapper mapper = new ModelMapper();
        mapper.map(pcaDTO.getReleasedBy(), UserDTO.class);
        PettycashAllocation pca = mapper.map(pcaDTO, PettycashAllocation.class);
        pca.setReleasedDate(new Date());
        PettycashAllocation retPCA = pcas.insert(pca);
        mapper.map(retPCA.getReleasedBy(), UserDTO.class);
        return mapper.map(retPCA, PCADTO.class);

    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestBody  PCADTO pcaDTO) {
        ModelMapper mapper = new ModelMapper();
        mapper.map(pcaDTO.getReleasedBy(), UserDTO.class);
        PettycashAllocation pca = mapper.map(pcaDTO, PettycashAllocation.class);
        pca.setReleasedDate(new Date());
        pcas.update(pca);
        return "success";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    @ResponseBody
    public String delete() {
        return "pettycash/pettyCashAllocation";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public PCADTO getById(@PathVariable("id") int id) {
        ModelMapper mapper = new ModelMapper();
        PettycashAllocation pca = pcas.getById(id);
        mapper.map(pca.getReleasedBy(), UserDTO.class);
        return mapper.map(pca, PCADTO.class);
    }

}
