/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.pettycash.DTO;

import com.puzan.user.DTO.UserDTO;
import java.util.Date;

/**
 *
 * @author puzan
 */
public class PCADTO {

    private Integer ptAllocationId;
    private double amount;
    private Date releasedDate;
    private UserDTO releasedBy;

    public Integer getPtAllocationId() {
        return ptAllocationId;
    }

    public void setPtAllocationId(Integer ptAllocationId) {
        this.ptAllocationId = ptAllocationId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getReleasedDate() {
        return releasedDate;
    }

    public void setReleasedDate(Date releasedDate) {
        this.releasedDate = releasedDate;
    }

    public UserDTO getReleasedBy() {
        return releasedBy;
    }

    public void setReleasedBy(UserDTO releasedBy) {
        this.releasedBy = releasedBy;
    }
    
    
}
