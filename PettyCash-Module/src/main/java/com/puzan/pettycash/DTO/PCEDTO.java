/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.pettycash.DTO;

import com.puzan.user.DTO.UserDTO;
import java.util.Date;

/**
 *
 * @author puzan
 */
public class PCEDTO {

    private Integer ptExpId;
    private Date expDate;
    private double amount;
    private UserDTO expBy;

    public Integer getPtExpId() {
        return ptExpId;
    }

    public void setPtExpId(Integer ptExpId) {
        this.ptExpId = ptExpId;
    }

    public Date getExpDate() {
        return expDate;
    }

    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public UserDTO getExpBy() {
        return expBy;
    }

    public void setExpBy(UserDTO expBy) {
        this.expBy = expBy;
    }
    
    
    
}
