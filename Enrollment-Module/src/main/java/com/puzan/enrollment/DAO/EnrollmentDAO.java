/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enrollment.DAO;

import com.puzan.core.DAO.GenericDAO;
import com.puzan.entity.Entities.Enrollment;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface EnrollmentDAO extends GenericDAO<Enrollment> {

    //used in dashboard component
    Long countTotalEnrollment();

    List<Enrollment> search(String query);

    List<Enrollment> getEnrollmentByBatchId(int batchId);
    
}
