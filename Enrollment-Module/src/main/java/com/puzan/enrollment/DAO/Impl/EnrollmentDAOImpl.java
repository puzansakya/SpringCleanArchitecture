/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enrollment.DAO.Impl;

import com.puzan.enrollment.DAO.EnrollmentDAO;
import com.puzan.entity.Entities.Enrollment;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "EnrollmentDAO")
public class EnrollmentDAOImpl implements EnrollmentDAO {

    @Autowired
    private SessionFactory sf;
    private Session session;
    private Transaction transaction;

    @Override
    public List<Enrollment> getAll() {
        session = sf.openSession();
        List<Enrollment> enrollmentList = session.createQuery("from Enrollment").list();
        session.close();
        return enrollmentList;
    }

    @Override
    public Enrollment insert(Enrollment t) {
        session = sf.openSession();
        transaction = session.beginTransaction();
        session.save(t);
        transaction.commit();
        session.close();
        return t;
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Enrollment t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Enrollment getById(int id) {
        session = sf.openSession();
        Enrollment enrollment = (Enrollment) session.get(Enrollment.class, id);
        session.close();
        return enrollment;
    }

    @Override
    public Long countTotalEnrollment() {
        session = sf.openSession();
        String hql = "SELECT count(1) FROM Enrollment";
        Query query = session.createQuery(hql);
        Long count = (Long) query.uniqueResult();
        session.close();
        return count;
    }

    @Override
    public List<Enrollment> search(String Keyword) {
        session = sf.openSession();
        String hql = "from Enrollment where studentId.firstName LIKE :searchKeyword";
        Query query = session.createQuery(hql);
        query.setParameter("searchKeyword", "%" + Keyword + "%");
        List<Enrollment> enrollmentList = query.list();
        session.close();
        return enrollmentList;
    }

    @Override
    public List<Enrollment> getEnrollmentByBatchId(int batchId) {
        session = sf.openSession();
        String hql = "from Enrollment where batchId.batchId =  :batchId";
        Query query = session.createQuery(hql);
        query.setParameter("batchId", batchId);
        List<Enrollment> enrollmentList = query.list();
        session.close();
        return enrollmentList;
    }
    

}
