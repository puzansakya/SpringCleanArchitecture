/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enrollment.DTO;

import com.puzan.batch.DTO.BatchDTO;
import com.puzan.batchcode.DTO.BatchCodeGeneratorDTO;
import com.puzan.course.DTO.CourseDTO;
import com.puzan.discount.DTO.DiscountDTO;
import com.puzan.entity.Entities.Batch;
import com.puzan.entity.Entities.Course;
import com.puzan.entity.Entities.Discount;
import com.puzan.entity.Entities.Student;
import com.puzan.entity.Entities.Time;
import com.puzan.student.DTO.StudentDTO;
import com.puzan.time.DTO.TimeDTO;
import java.util.Date;

/**
 *
 * @author puzan
 */
public class EnrollmentDTO {

    //required to export data as json
    private Integer enrollmentId;
    private String firstName;
    private String lastName;
    private String email;
    private int discount;
    private String time;
    private String batchCode;
    private String courseName;
    private int fees;
    private Date enrolledDate;
    private float amount;
    private float remAmount;
    private float paidPercentage;
    private String studentAvatar;

    public Integer getEnrollmentId() {
        return enrollmentId;
    }

    public void setEnrollmentId(Integer enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getFees() {
        return fees;
    }

    public void setFees(int fees) {
        this.fees = fees;
    }

    public Date getEnrolledDate() {
        return enrolledDate;
    }

    public void setEnrolledDate(Date enrolledDate) {
        this.enrolledDate = enrolledDate;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public float getRemAmount() {
        return remAmount;
    }

    public void setRemAmount(float remAmount) {
        this.remAmount = remAmount;
    }

    public float getPaidPercentage() {
        return paidPercentage;
    }

    public void setPaidPercentage(float paidPercentage) {
        this.paidPercentage = paidPercentage;
    }

    public String getStudentAvatar() {
        return studentAvatar;
    }

    public void setStudentAvatar(String studentAvatar) {
        this.studentAvatar = studentAvatar;
    }

    
}
