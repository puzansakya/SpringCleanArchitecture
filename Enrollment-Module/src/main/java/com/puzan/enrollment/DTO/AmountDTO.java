/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enrollment.DTO;

/**
 *
 * @author puzan
 */
public class AmountDTO {

    private int enrollmentId;
    private float amount;
    private float remAmount;
    private float paidPercentage;

    public float getRemAmount() {
        return remAmount;
    }

    public void setRemAmount(float remAmount) {
        this.remAmount = remAmount;
    }

    public float getPaidPercentage() {
        return paidPercentage;
    }

    public void setPaidPercentage(float paidPercentage) {
        this.paidPercentage = paidPercentage;
    }
   
    

    public int getEnrollmentId() {
        return enrollmentId;
    }

    public void setEnrollmentId(int enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

}
