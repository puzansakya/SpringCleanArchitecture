/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enrollment.Controller;

import com.puzan.batch.DTO.BatchDTO;
import com.puzan.batch.Service.BatchService;
import com.puzan.batchcode.DTO.BatchCodeGeneratorDTO;
import com.puzan.course.DTO.CourseDTO;
import com.puzan.course.Service.CourseService;
import com.puzan.discount.DTO.DiscountDTO;
import com.puzan.discount.Service.DiscountService;
import com.puzan.enquiry.Service.EnquiryService;
import com.puzan.enrollment.DTO.AmountDTO;
import com.puzan.enrollment.DTO.EnrollmentDTO;
import com.puzan.enrollment.DTO.EnrollmentSaveDTO;
import com.puzan.enrollment.Service.AmountService;
import com.puzan.enrollment.Service.EnrollmentService;
import com.puzan.enrollstatus.Service.EnrollStatusService;
import com.puzan.entity.Entities.Batch;
import com.puzan.entity.Entities.Course;
import com.puzan.entity.Entities.Discount;
import com.puzan.entity.Entities.Enquiry;
import com.puzan.entity.Entities.Enrollment;
import com.puzan.entity.Entities.Student;
import com.puzan.entity.Entities.Time;
import com.puzan.student.DTO.StudentDTO;
import com.puzan.student.Service.StudentService;
import com.puzan.time.DTO.TimeDTO;
import com.puzan.time.Service.TimeService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzan
 */
@Controller
@RequestMapping(value = "admin/enrollment")
@CrossOrigin
public class EnrollmentController {
    
    @Autowired
    private EnrollmentService enrollmentService;
    @Autowired
    private CourseService course;
    @Autowired
    private BatchService batch;
    @Autowired
    private DiscountService discount;
    @Autowired
    private TimeService time;
    @Autowired
    private StudentService student;
    @Autowired
    private AmountService as;
    @Autowired
    private EnquiryService enquiryService;
    @Autowired
    private EnrollStatusService ess;
    
    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        //model.addAttribute("enrollmentList", enrollmentService.getAll());
        model.addAttribute("active", "enrollment");
        return "Enrollment/index";
    }

    //get all the enrollments
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<EnrollmentDTO> getAll() {
        
        List<EnrollmentDTO> enrollmentDTOList = new ArrayList<>();
        int i = 0;
        for (Enrollment e : enrollmentService.getAll()) {
            EnrollmentDTO eDTO = new EnrollmentDTO();
            
            eDTO.setEnrollmentId(e.getEnrollmentId());
            eDTO.setFirstName(e.getStudentId().getFirstName());
            eDTO.setLastName(e.getStudentId().getLastName());
            eDTO.setEmail(e.getStudentId().getEmail());
            eDTO.setDiscount(e.getDiscountId().getDiscount());
            eDTO.setTime(e.getTimeId().getTime());
            eDTO.setBatchCode(e.getBatchId().getBatchCodeId().getBatchCode());
            eDTO.setCourseName(e.getCourseId().getCourseName());
            eDTO.setFees(e.getFees());
            eDTO.setEnrolledDate(e.getEnrolledDate());
            eDTO.setStudentAvatar(e.getStudentId().getStudentAvatar());
            //get Amount

            AmountDTO amount = as.getAllAmount().get(i);
            if (e.getEnrollmentId() == amount.getEnrollmentId()) {
                eDTO.setAmount(amount.getAmount());
                eDTO.setRemAmount(e.getFees() - amount.getAmount());
                eDTO.setPaidPercentage((amount.getAmount() / e.getFees() * 100));
            }
            
            enrollmentDTOList.add(eDTO);
            i++;
        }
        
        return enrollmentDTOList;
    }
    
    @RequestMapping(value = "/search/{keyword}", method = RequestMethod.GET)
    @ResponseBody
    public List<EnrollmentDTO> search(@PathVariable("keyword") String keyword) {
//        List<EnrollmentDTO> enrollmentDTOList = new ArrayList<>();
//
//        for (int i = 0; i < enrollmentService.search(keyword).size(); i++) {
//            ModelMapper mapper = new ModelMapper();
//            Enrollment enrollment = enrollmentService.getAll().get(i);
//            mapper.map(enrollment.getDiscountId(), DiscountDTO.class);
//            mapper.map(enrollment.getTimeId(), TimeDTO.class);
//            mapper.map(enrollment.getCourseId(), CourseDTO.class);
//            mapper.map(enrollment.getStudentId(), StudentDTO.class);
//            mapper.map(enrollment.getBatchId().getBatchCodeId(), BatchCodeGeneratorDTO.class);
//
//            EnrollmentDTO enrollmentDTO = mapper.map(enrollment, EnrollmentDTO.class);
//
//            AmountDTO amountDTO = as.getAllAmount().get(i);
//            amountDTO.setRemAmount(enrollmentDTO.getCourse().getFees() - amountDTO.getAmount());
//            amountDTO.setPaidPercentage((amountDTO.getAmount() / enrollmentDTO.getCourse().getFees() * 100));
//
//            if (enrollment.getEnrollmentId() == amountDTO.getEnrollmentId()) {
//                enrollmentDTO.setAmount(amountDTO);
//            }
//
//            enrollmentDTOList.add(enrollmentDTO);
//
//        }
//        return enrollmentDTOList;

        List<EnrollmentDTO> enrollmentDTOList = new ArrayList<>();
        for (Enrollment e : enrollmentService.search(keyword)) {
            EnrollmentDTO eDTO = new EnrollmentDTO();
            
            eDTO.setEnrollmentId(e.getEnrollmentId());
            eDTO.setFirstName(e.getStudentId().getFirstName());
            eDTO.setLastName(e.getStudentId().getLastName());
            eDTO.setEmail(e.getStudentId().getEmail());
            eDTO.setDiscount(e.getDiscountId().getDiscount());
            eDTO.setTime(e.getTimeId().getTime());
            eDTO.setBatchCode(e.getBatchId().getBatchCodeId().getBatchCode());
            eDTO.setCourseName(e.getCourseId().getCourseName());
            eDTO.setFees(e.getFees());
            eDTO.setEnrolledDate(e.getEnrolledDate());
            eDTO.setStudentAvatar(e.getStudentId().getStudentAvatar());
            //get Amount

            for (AmountDTO amount : as.getAllAmount()) {
                if (e.getEnrollmentId() == amount.getEnrollmentId()) {
                    eDTO.setAmount(amount.getAmount());
                    eDTO.setRemAmount(e.getCourseId().getFees() - amount.getAmount());
                    eDTO.setPaidPercentage((amount.getAmount() / e.getCourseId().getFees() * 100));
                }
            }
            
            enrollmentDTOList.add(eDTO);
        }
        
        return enrollmentDTOList;
    }
    
    @RequestMapping(value = "/store", method = RequestMethod.POST)
    @ResponseBody
    public String store(@RequestBody EnrollmentSaveDTO enq) {

        //get enquiry by id
        Enquiry e = enquiryService.getById(enq.getEnquiryId());

        //check if student exist or not by email obtained from enquiry 
        //if user exist get that student else create new student
        Student s = student.getByEmail(e.getEmail());
        if (s == null) {
            s = new Student();
            s.setFirstName(e.getFirstname());
            s.setLastName(e.getLastname());
            s.setEmail(e.getEmail());
            s.setContactNo(e.getContactNo());
            s.setStatus(true);
            s.setStudentAvatar(enq.getStudentAvatar());
        }

        //Create new Enrollment
        Enrollment enrollment = new Enrollment();

        //Assign the above obtained student
        enrollment.setStudentId(s);

        //Assign discount
        Discount d = discount.getById(enq.getDiscountId());
        enrollment.setDiscountId(d);

        //Assign time
        enrollment.setTimeId(time.getById(enq.getTimeId()));

        //obtain course from enquiry and assign course
        Course c = course.getById(enq.getCourseId());
        enrollment.setCourseId(c);

        //Assign batch
        enrollment.setBatchId(batch.getById(enq.getBatchId()));

        //Assign amount
        enrollment.setStatus(true);

        //Assign fees
        int rem_dis = 100 - d.getDiscount();
        int original_fee = c.getFees();
        int new_fee = (rem_dis * original_fee) / 100;
        enrollment.setFees(new_fee);

        //insert enrollment
        if (enrollmentService.insert(enrollment) != null) {
            //set enquiry status to active
            e.setEnrollStatusId(ess.getById(2));

            //update enquiry
            enquiryService.update(e);
            return "[{\"response\":\"success\"}]";
        }
        
        return "[{\"response\":\"failed\"}]";
    }
    
    @RequestMapping(value = "/countTotalEnrollment", method = RequestMethod.GET)
    public @ResponseBody
    String countTotalEnrollment() {
        return String.valueOf(enrollmentService.countTotalEnrollment());
    }
    
    @RequestMapping(value = "/getEnrollmentByBatchId/{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<EnrollmentDTO> getEnrollmentByBatchId(@PathVariable("id") int id) {
        List<EnrollmentDTO> enrollmentDTOList = new ArrayList<>();
        
        for (int i = 0; i < enrollmentService.getEnrollmentByBatchId(id).size(); i++) {
            ModelMapper mapper = new ModelMapper();
            Enrollment enrollment = enrollmentService.getAll().get(i);
            mapper.map(enrollment.getDiscountId(), DiscountDTO.class);
            mapper.map(enrollment.getTimeId(), TimeDTO.class);
            mapper.map(enrollment.getCourseId(), CourseDTO.class);
            mapper.map(enrollment.getStudentId(), StudentDTO.class);
            mapper.map(enrollment.getBatchId().getBatchCodeId(), BatchCodeGeneratorDTO.class);
            
            EnrollmentDTO enrollmentDTO = mapper.map(enrollment, EnrollmentDTO.class);
            enrollmentDTO.setFirstName(enrollment.getStudentId().getFirstName());
            enrollmentDTO.setLastName(enrollment.getStudentId().getLastName());
            enrollmentDTO.setEmail(enrollment.getStudentId().getEmail());
            
            enrollmentDTOList.add(enrollmentDTO);
            
        }
        return enrollmentDTOList;
    }
    
}
