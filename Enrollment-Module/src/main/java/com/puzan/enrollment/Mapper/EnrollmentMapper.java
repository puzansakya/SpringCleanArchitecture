/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enrollment.Mapper;

import com.puzan.course.Mapper.CourseMapper;
import com.puzan.discount.Mapper.DiscountMapper;
import com.puzan.enrollment.DTO.EnrollmentDTO;
import com.puzan.entity.Entities.Enrollment;
import com.puzan.student.Mapper.StudentMapper;
import com.puzan.time.Mapper.TimeMapper;

/**
 *
 * @author puzan
 */
public class EnrollmentMapper {

    DiscountMapper discountMapper;
    TimeMapper timeMapper;
    CourseMapper courseMapper;
    StudentMapper studentMapper;

    public EnrollmentMapper() {
        discountMapper = new DiscountMapper();
        timeMapper = new TimeMapper();
        courseMapper = new CourseMapper();
        studentMapper = new StudentMapper();
    }

    //convert entity to dto
    public EnrollmentDTO entityToDto(Enrollment e) {
        //create new EnrollmentDTO
//        EnrollmentDTO eDTO = new EnrollmentDTO();
//        
//        eDTO.setEnrollmentId(e.getEnrollmentId());
//        eDTO.setFees(e.getFees());
//        eDTO.setEnrolledDate(e.getEnrolledDate());
//        eDTO.setDiscount(discountMapper.entityToDto(e.getDiscountId()));
//        eDTO.setTime(timeMapper.entityToDto(e.getTimeId()));
//        eDTO.setBatch(e.getFees());
//        eDTO.setCourse(courseMapper.entityToDto(e.getCourseId()));
//        eDTO.setStudent(studentMapper.entityToDto(e.getStudentId()));
//        eDTO.setStatus(e.getStatus());
        return null;

    }

    //convert dto to entity
    public Enrollment DtoToEntity(EnrollmentDTO eDTO) {
        return null;
    }

}
