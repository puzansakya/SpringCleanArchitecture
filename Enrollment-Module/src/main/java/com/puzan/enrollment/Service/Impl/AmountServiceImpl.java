/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enrollment.Service.Impl;

import com.puzan.enrollment.DAO.AmountDAO;
import com.puzan.enrollment.DTO.AmountDTO;
import com.puzan.enrollment.Service.AmountService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "AmountService")
public class AmountServiceImpl implements AmountService {

    @Autowired
    private AmountDAO ad;

    @Override
    public List<AmountDTO> getAllAmount() {
        return ad.getAllAmount();
    }

}
