/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enrollment.Service.Impl;

import com.puzan.enrollment.DAO.EnrollmentDAO;
import com.puzan.enrollment.Service.EnrollmentService;
import com.puzan.entity.Entities.Enrollment;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "EnrollmentService")
public class EnrollmentServiceImpl implements EnrollmentService {

    @Autowired
    private EnrollmentDAO ed;

    @Override
    public List<Enrollment> getAll() {
        return ed.getAll();
    }

    @Override
    public Enrollment insert(Enrollment t) {
        return ed.insert(t);
    }

    @Override
    public void delete(int id) {
       ed.delete(id);
    }

    @Override
    public void update(Enrollment t) {
        ed.update(t);
    }

    @Override
    public Enrollment getById(int id) {
        return ed.getById(id);
    }

    @Override
    public Long countTotalEnrollment() {
        return ed.countTotalEnrollment();
    }

    @Override
    public List<Enrollment> search(String keyword) {
        return ed.search(keyword);
    }

    @Override
    public List<Enrollment> getEnrollmentByBatchId(int batchId) {
        return ed.getEnrollmentByBatchId(batchId);
    }
  
}
