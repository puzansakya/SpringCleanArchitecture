/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enrollment.Service;

import com.puzan.core.Service.GenericService;
import com.puzan.entity.Entities.Enrollment;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface EnrollmentService extends GenericService<Enrollment> {

    Long countTotalEnrollment();

    List<Enrollment> search(String keyword);

    List<Enrollment> getEnrollmentByBatchId(int batchId);
    
}
