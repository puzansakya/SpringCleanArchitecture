/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.branch.DAO;

import com.puzan.core.DAO.GenericDAO;
import com.puzan.entity.Entities.Branch;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface BranchDAO extends GenericDAO<Branch> {

    List<Branch> search(String keyword);
}
