/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.branch.DAO.Impl;

import com.puzan.branch.DAO.BranchDAO;
import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.entity.Entities.Branch;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "BranchDAO")
public class BranchDAOImpl extends GenericDAOImpl<Branch> implements BranchDAO {

    @Override
    public List<Branch> search(String keyword) {
        session = sessionFactory.openSession();
        List<Branch> b = session.createQuery("from Branch where branchName = :keyword").setParameter("keyword", keyword).list();
        session.close();
        return b;
    }

}
