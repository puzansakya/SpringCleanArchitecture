/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.branch.DTO;

import java.util.Date;

/**
 *
 * @author puzan
 */
public class BranchDTO {

    private Integer branchId;
    private String branchName;
    private String branchLocation;
    private boolean branchStatus;
    private Date addedDate;
    private Date modifiedDate;

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchLocation() {
        return branchLocation;
    }

    public void setBranchLocation(String branchLocation) {
        this.branchLocation = branchLocation;
    }

    public boolean isBranchStatus() {
        return branchStatus;
    }

    public void setBranchStatus(boolean branchStatus) {
        this.branchStatus = branchStatus;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public String toString() {
        return "BranchDTO{" + "branchId=" + branchId + ", branchName=" + branchName + ", branchLocation=" + branchLocation + ", branchStatus=" + branchStatus + ", addedDate=" + addedDate + ", modifiedDate=" + modifiedDate + '}';
    }

}
