/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.branch.Controller;

import com.puzan.branch.DAO.BranchDAO;
import com.puzan.branch.DTO.BranchDTO;
import com.puzan.branch.Service.BranchService;
import com.puzan.entity.Entities.Branch;
import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
@RequestMapping(value = "/admin/branch")
public class BranchController {

    @Autowired
    private BranchService bs;

    private ModelMapper mapper;

    public BranchController() {
        mapper = new ModelMapper();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("active", "branch");
        return "Branch/index";
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<BranchDTO> getAll() {
        List<BranchDTO> b = new ArrayList<>();
        for (Branch branch : bs.getAll()) {
            b.add(mapper.map(branch, BranchDTO.class));
        }
        return b;
    }

    @RequestMapping(value = "/search/{keyword}", method = RequestMethod.GET)
    @ResponseBody
    public List<BranchDTO> search(@PathVariable("keyword") String keyword) {
        List<BranchDTO> b = new ArrayList<>();
        for (Branch branch : bs.search(keyword)) {
            b.add(mapper.map(branch, BranchDTO.class));
        }
        return b;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public BranchDTO save(@RequestBody BranchDTO b) {
        Branch branch = bs.insert(mapper.map(b, Branch.class));
        return mapper.map(branch, BranchDTO.class);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestBody BranchDTO b) {
        bs.update(mapper.map(b, Branch.class));
        return "[{\"response\":\"success\"}]";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String delete(@PathVariable("id") int id) {
        return "[{\"response\":\"null\"}]";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public BranchDTO getById(@PathVariable("id") int id) {
        Branch branch = bs.getById(id);
        return mapper.map(branch, BranchDTO.class);
    }
}
