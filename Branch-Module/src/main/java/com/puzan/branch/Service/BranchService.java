/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.branch.Service;

import com.puzan.core.Service.GenericService;
import com.puzan.entity.Entities.Branch;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface BranchService extends GenericService<Branch>{
    
    List<Branch> search(String keyword);
    
}
