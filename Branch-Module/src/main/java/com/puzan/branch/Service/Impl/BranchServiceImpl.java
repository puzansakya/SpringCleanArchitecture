/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.branch.Service.Impl;

import com.puzan.branch.DAO.BranchDAO;
import com.puzan.branch.Service.BranchService;
import com.puzan.entity.Entities.Branch;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "BranchService")
public class BranchServiceImpl implements BranchService {

    @Autowired
    private BranchDAO bd;

    @Override
    public List<Branch> getAll() {
        return bd.getAll();
    }

    @Override
    public Branch insert(Branch t) {
        return bd.insert(t);
    }

    @Override
    public void delete(int id) {
        bd.delete(id);
    }

    @Override
    public void update(Branch t) {
        bd.update(t);
    }

    @Override
    public Branch getById(int id) {
        return bd.getById(id);
    }

    @Override
    public List<Branch> search(String keyword) {
        return bd.search(keyword);
    }

}
