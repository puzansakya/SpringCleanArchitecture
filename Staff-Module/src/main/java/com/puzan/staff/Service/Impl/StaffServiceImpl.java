/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.staff.Service.Impl;

import com.puzan.entity.Entities.Staff;
import com.puzan.staff.DAO.StaffDAO;
import com.puzan.staff.Service.StaffService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "StaffService")
public class StaffServiceImpl implements StaffService {

    @Autowired
    private StaffDAO sd;

    @Override
    public List<Staff> search(String keyword) {
        return sd.search(keyword);
    }

    @Override
    public List<Staff> getAll() {
        return sd.getAll();
    }

    @Override
    public Staff insert(Staff t) {
        return sd.insert(t);
    }

    @Override
    public void delete(int id) {
        sd.delete(id);
    }

    @Override
    public void update(Staff t) {
        sd.update(t);
    }

    @Override
    public Staff getById(int id) {
        return sd.getById(id);
    }

}
