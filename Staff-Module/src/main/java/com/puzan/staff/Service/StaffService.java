/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.staff.Service;


import com.puzan.core.Service.GenericService;
import com.puzan.entity.Entities.Staff;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface StaffService extends GenericService<Staff> {

    List<Staff> search(String keyword);
}
