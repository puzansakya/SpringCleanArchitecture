/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.staff.DTO;


import com.puzan.branch.DTO.BranchDTO;
import com.puzan.user.DTO.UserDTO;
import java.util.Date;

/**
 *
 * @author puzan
 */
public class StaffDTO {

    private Integer branchUserId;
    private Date addedDate;
    private Date modifiedDate;
    private boolean status;
    private UserDTO user;
    private BranchDTO branch;

    public Integer getBranchUserId() {
        return branchUserId;
    }

    public void setBranchUserId(Integer branchUserId) {
        this.branchUserId = branchUserId;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public BranchDTO getBranch() {
        return branch;
    }

    public void setBranch(BranchDTO branch) {
        this.branch = branch;
    }

    @Override
    public String toString() {
        return "StaffDTO{" + "branchUserId=" + branchUserId + ", addedDate=" + addedDate + ", modifiedDate=" + modifiedDate + ", status=" + status + ", user=" + user + ", branch=" + branch + '}';
    }

}
