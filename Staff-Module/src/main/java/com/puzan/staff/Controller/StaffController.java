/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.staff.Controller;

import com.puzan.branch.DTO.BranchDTO;
import com.puzan.entity.Entities.Staff;
import com.puzan.staff.DTO.StaffDTO;
import com.puzan.staff.Service.StaffService;
import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzan
 */
@Controller
@CrossOrigin
@RequestMapping(value = "/admin/staff")
public class StaffController {

    @Autowired
    private StaffService ss;

    private ModelMapper mapper;

    public StaffController() {

        mapper = new ModelMapper();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
model.addAttribute("active","staff");
        return "Staff/index";
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<StaffDTO> getAll() {
        List<StaffDTO> staff = new ArrayList<>();
        for (Staff s : ss.getAll()) {
            mapper.map(s.getBranchId(),BranchDTO.class);
            staff.add(mapper.map(s, StaffDTO.class));
        }
        return staff;
    }

    @RequestMapping(value = "/search/{keyword}", method = RequestMethod.GET)
    @ResponseBody
    public List<StaffDTO> search(@PathVariable("keyword") String keyword) {
        List<StaffDTO> staff = new ArrayList<>();
        for (Staff s : ss.search(keyword)) {
            staff.add(mapper.map(s, StaffDTO.class));
        }
        return staff;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public StaffDTO save(StaffDTO s) {
        Staff staff = mapper.map(s, Staff.class);
        Staff retStaff = ss.insert(staff);
        StaffDTO retStaffDTO = mapper.map(retStaff, StaffDTO.class);
        return retStaffDTO;

    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String update(StaffDTO s) {
        Staff staff = mapper.map(s, Staff.class);
        ss.update(staff);
        return "[{\"response\":\"success\"}]";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String delete(@PathVariable("id") int id) {
        ss.delete(id);
        return "[{\"response\":\"success\"}]";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public StaffDTO getById(@PathVariable("id") int id) {
        Staff staff = ss.getById(id);
        return mapper.map(staff, StaffDTO.class);
    }
}
