/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.staff.DAO.Impl;

import com.puzan.branch.DAO.BranchDAO;
import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.entity.Entities.Branch;
import com.puzan.entity.Entities.Staff;
import com.puzan.staff.DAO.StaffDAO;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "StaffDAO")
public class StaffDAOImpl extends GenericDAOImpl<Staff> implements StaffDAO {

    @Override
    public List<Staff> search(String keyword) {
        session = sessionFactory.openSession();
        List<Staff> staff = session.createQuery("from Staff where username.username = :keyword").setParameter("keyword", keyword).list();
        session.close();
        return staff;
    }

}
