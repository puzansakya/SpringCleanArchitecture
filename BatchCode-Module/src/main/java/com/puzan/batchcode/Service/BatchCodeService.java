/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.batchcode.Service;

import com.puzan.core.Service.GenericService;
import com.puzan.entity.Entities.BatchCodeGenerator;


/**
 *
 * @author puzan
 */
public interface BatchCodeService extends GenericService<BatchCodeGenerator> {

    Long count(int course_id);
}
