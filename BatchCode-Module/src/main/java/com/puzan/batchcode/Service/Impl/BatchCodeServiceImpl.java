/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.batchcode.Service.Impl;

import com.puzan.batchcode.DAO.BatchCodeDAO;
import com.puzan.batchcode.Service.BatchCodeService;
import com.puzan.entity.Entities.BatchCodeGenerator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "BatchCodeService")
public class BatchCodeServiceImpl implements BatchCodeService {

    @Autowired
    private BatchCodeDAO bd;

    @Override
    public List<BatchCodeGenerator> getAll() {
        return bd.getAll();
    }

    @Override
    public BatchCodeGenerator insert(BatchCodeGenerator t) {
        return bd.insert(t);
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(BatchCodeGenerator t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BatchCodeGenerator getById(int id) {
        return bd.getById(id);
    }

    @Override
    public Long count(int course_id) {
        return bd.count(course_id);
    }

}
