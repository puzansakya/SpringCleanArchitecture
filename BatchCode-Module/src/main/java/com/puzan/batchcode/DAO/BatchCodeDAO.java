/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.batchcode.DAO;

import com.puzan.core.DAO.GenericDAO;
import com.puzan.entity.Entities.BatchCodeGenerator;



/**
 *
 * @author puzan
 */
public interface BatchCodeDAO extends GenericDAO<BatchCodeGenerator> {

    Long count(int course_id);

}
