/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.batchcode.DAO.Impl;


import com.puzan.batchcode.DAO.BatchCodeDAO;
import com.puzan.entity.Entities.BatchCodeGenerator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "BatchCodeDAO")
public class BatchCodeDAOImpl implements BatchCodeDAO {

    @Autowired
    private SessionFactory sf;
    private Session session;
    private Transaction transaction;

    @Override
    public Long count(int course_id) {
        session = sf.openSession();
        String hql = "select count(*) from BatchCodeGenerator  where course_id=:courseId";
        Query query = session.createQuery(hql);
        query.setParameter("courseId", course_id);
        Long count = (Long) query.uniqueResult();
        session.close();
        return count;
    }

    @Override
    public List<BatchCodeGenerator> getAll() {
        session = sf.openSession();
        List<BatchCodeGenerator> bcg = session.createQuery("from BatchCodeGenerator").list();
        session.close();
        return bcg;
    }

    @Override
    public BatchCodeGenerator insert(BatchCodeGenerator t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(BatchCodeGenerator t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BatchCodeGenerator getById(int id) {
        session = sf.openSession();
        BatchCodeGenerator bcg = (BatchCodeGenerator) session.get(BatchCodeGenerator.class, id);
        session.close();
        return bcg;
    }

}
