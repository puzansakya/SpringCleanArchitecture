/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.batch.Controller;

import com.puzan.batch.DTO.BatchDTO;
import com.puzan.batch.Helper.FileHelper;
import com.puzan.batch.Service.BatchService;
import com.puzan.batchcode.DTO.BatchCodeGeneratorDTO;
import com.puzan.batchcode.Service.BatchCodeService;
import com.puzan.course.DTO.CourseDTO;
import com.puzan.course.Service.CourseService;
import com.puzan.entity.Entities.Batch;
import com.puzan.entity.Entities.BatchCodeGenerator;
import com.puzan.entity.Entities.Course;
import com.puzan.facilator.DTO.FacultyDTO;
import com.puzan.facilator.Service.FacultyService;
import com.puzan.time.DTO.TimeDTO;
import com.puzan.time.Service.TimeService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author puzan
 */
@Controller
@RequestMapping(value = "admin/batch")
@CrossOrigin
public class BatchController {

    private FileHelper helper;
    @Autowired
    private CourseService course;
    @Autowired
    private BatchService bs;
    @Autowired
    private FacultyService faculty;
    @Autowired
    private BatchCodeService batchCode;
    @Autowired
    private TimeService ts;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("batchList", bs.getAll());
        model.addAttribute("courseList", course.getAll());
        model.addAttribute("facultyList", faculty.getAll());
        model.addAttribute("batchCodeList", batchCode.getAll());
        model.addAttribute("timeList", ts.getAll());
        model.addAttribute("active", "batch");
        return "batch/index";
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<BatchDTO> getAll() {
        List<BatchDTO> batchDTOList = new ArrayList<>();
        for (Batch batch : bs.getAll()) {
            ModelMapper mapper = new ModelMapper();
            mapper.map(batch.getCourseId(), CourseDTO.class);
            mapper.map(batch.getBatchCodeId(), BatchCodeGeneratorDTO.class);
            mapper.map(batch.getTimeId(), TimeDTO.class);
            mapper.map(batch.getFacultyId(), FacultyDTO.class);
            batchDTOList.add(mapper.map(batch, BatchDTO.class));
        }
        return batchDTOList;
    }

    @RequestMapping(value = "/search/{keyword}", method = RequestMethod.GET)
    @ResponseBody
    public List<BatchDTO> search(@PathVariable("keyword") String keyword) {
        List<BatchDTO> batchDTOList = new ArrayList<>();
        for (Batch batch : bs.search(keyword)) {
            ModelMapper mapper = new ModelMapper();
            mapper.map(batch.getCourseId(), CourseDTO.class);
            mapper.map(batch.getBatchCodeId(), BatchCodeGeneratorDTO.class);
            mapper.map(batch.getTimeId(), TimeDTO.class);
            mapper.map(batch.getFacultyId(), FacultyDTO.class);
            batchDTOList.add(mapper.map(batch, BatchDTO.class));
        }
        return batchDTOList;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public BatchDTO save(@RequestBody BatchDTO b) {
        helper = new FileHelper();
        Course courses = course.getById(b.getCourse().getCourseId());
        Batch batch = new Batch();

        MultipartFile file = b.getFile();
        if (file != null) {
            batch.setAgreement(file.getOriginalFilename());
        }

        BatchCodeGenerator bcg = new BatchCodeGenerator();
        bcg.setBatchCode(courses.getCode() + "-17-0" + String.valueOf((batchCode.count(courses.getCourseId()) + 1)));
        bcg.setCourseId(courses);
        bcg.setDate(new Date());

        batch.setCourseId(courses);
        batch.setTimeId(ts.getById(b.getTime().getTimeId()));
        batch.setFacultyId(faculty.getById(b.getFaculty().getFacultyId()));
        batch.setStatus(b.isStatus());
        batch.setPaymentStatus(b.isPaymentStatus());
        batch.setBatchCodeId(bcg);

        Batch retBatch = bs.insert(batch);

        if (retBatch != null) {
            if (file != null) {
                helper.UploadFile(file);
            }
        }
        ModelMapper mapper = new ModelMapper();
        mapper.map(batch.getCourseId(), CourseDTO.class);
        mapper.map(batch.getBatchCodeId(), BatchCodeGeneratorDTO.class);
        mapper.map(batch.getTimeId(), TimeDTO.class);
        mapper.map(batch.getFacultyId(), FacultyDTO.class);
        return mapper.map(batch, BatchDTO.class);
    }

}
