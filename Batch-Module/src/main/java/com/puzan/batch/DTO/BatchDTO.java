/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.batch.DTO;

import com.puzan.batchcode.DTO.BatchCodeGeneratorDTO;
import com.puzan.course.DTO.CourseDTO;
import com.puzan.facilator.DTO.FacultyDTO;
import com.puzan.time.DTO.TimeDTO;
import java.util.Date;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author puzan
 */
public class BatchDTO {
    private Integer batchId;
    private Date createdDate;
    private Date startDate;
    private Date endDate;
    private boolean status;
    private boolean paymentStatus;
    private String agreement;
    private MultipartFile file;
    
    private BatchCodeGeneratorDTO batchCode;
    private CourseDTO course;
    private TimeDTO time;
    private FacultyDTO faculty;
    

    public Integer getBatchId() {
        return batchId;
    }

    public void setBatchId(Integer batchId) {
        this.batchId = batchId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getAgreement() {
        return agreement;
    }

    public void setAgreement(String agreement) {
        this.agreement = agreement;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public BatchCodeGeneratorDTO getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(BatchCodeGeneratorDTO batchCode) {
        this.batchCode = batchCode;
    }

    public CourseDTO getCourse() {
        return course;
    }

    public void setCourse(CourseDTO course) {
        this.course = course;
    }

    public TimeDTO getTime() {
        return time;
    }

    public void setTime(TimeDTO time) {
        this.time = time;
    }

    public FacultyDTO getFaculty() {
        return faculty;
    }

    public void setFaculty(FacultyDTO faculty) {
        this.faculty = faculty;
    }
    
    
    
}
