/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.batch.Service.Impl;

import com.puzan.batch.DAO.BatchDAO;
import com.puzan.batch.Helper.FileHelper;
import com.puzan.batch.Service.BatchService;
import com.puzan.batchcode.DAO.BatchCodeDAO;
import com.puzan.course.DAO.CourseDAO;
import com.puzan.entity.Entities.Batch;
import com.puzan.facilator.DAO.FacultyDAO;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "BatchService")
public class BatchServiceImpl implements BatchService {

    private FileHelper helper;
    @Autowired
    private BatchDAO b;
    @Autowired
    private CourseDAO course;
    @Autowired
    private FacultyDAO faculty;
    @Autowired
    private BatchCodeDAO batchCode;

    @Override
    public List<Batch> getAll() {
        return b.getAll();
    }

    @Override
    public Batch insert(Batch batch) {

//        helper = new FileHelper();
//
//        MultipartFile file = batch.getFile();
//
//        Faculties f = faculty.getById(batch.getFaculties_id());
//        Course c = course.getById(batch.getCourse_id());
//
//        int course_id = batch.getCourse_id();
//        //existing batch code works well
//        // BatchCodeGenerator bcg = batchCode.getById(b.getBatchCodeGenerator_id());
//        //create new batch code does not work
//        BatchCodeGenerator bcg = new BatchCodeGenerator();
//        if (course_id == 2) {
//            bcg.setBatchCode("AJV-17-00" + String.valueOf((batchCode.count(course_id) + 1)));
//        } else {
//            bcg.setBatchCode("PHPJS-17-00" + String.valueOf((batchCode.count(course_id) + 1)));
//        }
//        bcg.setCourse(c);
//        bcg.setDate(new Date());
//
//        Batch newBatch = new Batch();
//        newBatch.setCreatedDate(new Date());
//        newBatch.setStartDate(new Date());
//        newBatch.setEndDate(null);
//        newBatch.setStatus(batch.getStatus());
//        newBatch.setPaymentStatus(batch.getPaymentStatus());
//        newBatch.setAgreement(file.getOriginalFilename());
//
//        newBatch.setCourse(c);
//        newBatch.setBatchCodeGenerator(bcg);
//        newBatch.setFaculties(f);
//
//        int result = b.insert(newBatch);
//
//        if (result > 0) {
//            helper.UploadFile(file);
//        }
//        return result;
        return b.insert(batch);
    }

    @Override
    public void delete(int id) {
        b.delete(id);
    }

    @Override
    public void update(Batch t) {
        b.update(t);
    }

    @Override
    public Batch getById(int id) {
        return b.getById(id);
    }

    @Override
    public List<Batch> search(String keyword) {
        return b.search(keyword);
    }

}
