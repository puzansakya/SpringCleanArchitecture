/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.batch.Service;

import com.puzan.core.Service.GenericService;
import com.puzan.entity.Entities.Batch;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface BatchService extends GenericService<Batch>{
    List<Batch> search(String keyword);
}
