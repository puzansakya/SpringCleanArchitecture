/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.batch.DAO;


import com.puzan.core.DAO.GenericDAO;
import com.puzan.entity.Entities.Batch;
import java.util.List;


/**
 *
 * @author puzan
 */
public interface BatchDAO extends GenericDAO<Batch> {
    
    List<Batch> search(String keyword);
    
}
