/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.batch.DAO.Impl;

import com.puzan.batch.DAO.BatchDAO;
import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.entity.Entities.Batch;
import java.util.List;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "BatchDAO")
public class BatchDAOImpl extends GenericDAOImpl<Batch> implements BatchDAO {

    @Override
    public List<Batch> search(String keyword) {
         String hql = "from Enrollment where courseId.courseName LIKE :searchKeyword";
        Query query = session.createQuery(hql);
        query.setParameter("searchKeyword", "%" + keyword + "%");
        List<Batch> enrollmentList = query.list();
        session.close();
        return enrollmentList;
    }

    
}
