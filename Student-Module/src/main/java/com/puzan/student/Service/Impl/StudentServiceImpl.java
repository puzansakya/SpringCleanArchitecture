/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.student.Service.Impl;

import com.puzan.entity.Entities.Student;
import com.puzan.student.DAO.StudentDAO;
import com.puzan.student.Service.StudentService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "studentService")
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDAO sd;

    @Override
    public List<Student> getAll() {
        return sd.getAll();
    }

    @Override
    public Student insert(Student t) {
        return sd.insert(t);
    }

    @Override
    public void delete(int id) {
        sd.delete(id);
    }

    @Override
    public void update(Student t) {
        sd.update(t);
    }

    @Override
    public Student getById(int id) {
        return sd.getById(id);
    }

    @Override
    public Student getByEmail(String email) {
        return sd.getByEmail(email);
    }

    @Override
    public List<Student> search(String keyword) {
        return sd.search(keyword);
    }

}
