/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.student.Service;

import com.puzan.core.Service.GenericService;
import com.puzan.entity.Entities.Student;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface StudentService extends GenericService<Student> {

    Student getByEmail(String email);

    List<Student> search(String keyword);

}
