/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.student.Controller;


import com.puzan.entity.Entities.Student;
import com.puzan.student.DTO.StudentDTO;
import com.puzan.student.Service.StudentService;
import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzan
 */
@Controller
@RequestMapping(value = "/admin/student")
@CrossOrigin(maxAge = 3600)
public class StudentController {

    @Autowired
    private StudentService ss;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String index(Model model) {
        model.addAttribute("active", "student");
        return "Student:index";
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<StudentDTO> getAll() {
        ModelMapper mapper = new ModelMapper();
        List<StudentDTO> studentDTOList = new ArrayList<>();
        for (Student student : ss.getAll()) {
            studentDTOList.add(mapper.map(student, StudentDTO.class));
        }
        return studentDTOList;
    }

    @RequestMapping(value = "/search/{keyword}", method = RequestMethod.GET)
    @ResponseBody
    public List<StudentDTO> search(@PathVariable("keyword") String keyword) {
        ModelMapper mapper = new ModelMapper();
        List<StudentDTO> studentDTOList = new ArrayList<>();
        for (Student student : ss.search(keyword)) {
            studentDTOList.add(mapper.map(student, StudentDTO.class));
        }
        return studentDTOList;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public StudentDTO save(@RequestBody StudentDTO s) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(ss.insert(mapper.map(s, Student.class)), StudentDTO.class);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String update(StudentDTO s) {
        ModelMapper mapper = new ModelMapper();
        ss.update(mapper.map(s, Student.class));
        return "success";

    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String delete(@PathVariable("id") int id) {
        ss.delete(id);
        return "deleted";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public StudentDTO getById(@PathVariable("id") int id) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(ss.getById(id), StudentDTO.class);
    }
}
