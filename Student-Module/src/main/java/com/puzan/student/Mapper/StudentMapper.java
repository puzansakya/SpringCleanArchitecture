/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.student.Mapper;

import com.puzan.entity.Entities.Student;
import com.puzan.student.DTO.StudentDTO;



/**
 *
 * @author puzan
 */
public class StudentMapper {

    //convert entity to dto
    public StudentDTO entityToDto(Student s) {
        //create new CourseDTO
        StudentDTO sDTO = new StudentDTO();
        sDTO.setStudentId(s.getStudentId());
        sDTO.setFirstName(s.getFirstName());
        sDTO.setLastName(s.getLastName());
        sDTO.setEmail(s.getEmail());
        sDTO.setContactNo(s.getContactNo());
        sDTO.setCreatedDate(s.getCreatedDate());
        sDTO.setModifiedDate(s.getModifiedDate());

        return sDTO;

    }

    //convert dto to entity
    public Student DtoToEntity(StudentDTO sDTO) {
        //create new CourseDTO
        Student s = new Student();
        s.setStudentId(sDTO.getStudentId());
        s.setFirstName(sDTO.getFirstName());
        s.setLastName(sDTO.getLastName());
        s.setEmail(sDTO.getEmail());
        s.setContactNo(sDTO.getContactNo());
        s.setCreatedDate(sDTO.getCreatedDate());
        s.setModifiedDate(sDTO.getModifiedDate());

        return s;
    }
}
