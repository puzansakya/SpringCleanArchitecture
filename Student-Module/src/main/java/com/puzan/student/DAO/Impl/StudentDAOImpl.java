/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.student.DAO.Impl;

import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.entity.Entities.Student;
import com.puzan.student.DAO.StudentDAO;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "StudentDAO")
public class StudentDAOImpl extends GenericDAOImpl<Student> implements StudentDAO {

    @Override
    public Student getByEmail(String email) {
        session = sessionFactory.openSession();
        String hql = "from Student  where email=:email";
        Query query = session.createQuery(hql);
        query.setParameter("email", email);
        List<Student> studentList = query.list();
        session.close();

        for (Student s : studentList) {
            return s;
        }
        return null;
    }

    @Override
    public List<Student> search(String keyword) {
        session = sessionFactory.openSession();
        Query query = session.createQuery("from Student where firstName LIKE :keyword");
        query.setParameter("keyword", "%" + keyword + "%");
        List<Student> studentList = query.list();
        session.close();
        return studentList;
    }

}
