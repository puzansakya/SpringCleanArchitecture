/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.student.DAO;

import com.puzan.core.DAO.GenericDAO;
import com.puzan.entity.Entities.Student;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface StudentDAO extends GenericDAO<Student> {

    Student getByEmail(String email);

    List<Student> search(String keyword);

}
