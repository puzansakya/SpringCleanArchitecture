/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.statistics.Entities;

import com.puzan.payment.DTO.PaymentStatDTO;
import java.util.List;

/**
 *
 * @author puzan
 */
public class StatsDTO {
    private int pendingEnquiry;
    private float bounceRate;
    private int totalEnrollment;
    private List<PaymentStatDTO> paymentStat;

    public List<PaymentStatDTO> getPaymentStat() {
        return paymentStat;
    }

    public void setPaymentStat(List<PaymentStatDTO> paymentStat) {
        this.paymentStat = paymentStat;
    }
    
    public int getPendingEnquiry() {
        return pendingEnquiry;
    }

    public void setPendingEnquiry(int pendingEnquiry) {
        this.pendingEnquiry = pendingEnquiry;
    }

    public float getBounceRate() {
        return bounceRate;
    }

    public void setBounceRate(float bounceRate) {
        this.bounceRate = bounceRate;
    }

    public int getTotalEnrollment() {
        return totalEnrollment;
    }

    public void setTotalEnrollment(int totalEnrollment) {
        this.totalEnrollment = totalEnrollment;
    }
    
    
    
}
