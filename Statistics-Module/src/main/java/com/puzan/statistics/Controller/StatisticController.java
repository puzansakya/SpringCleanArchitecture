/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.statistics.Controller;

import com.puzan.enquiry.Service.EnquiryService;
import com.puzan.enrollment.Service.EnrollmentService;
import com.puzan.entity.Entities.Payment;
import com.puzan.payment.DTO.PaymentStatDTO;
import com.puzan.payment.Service.PaymentService;
import com.puzan.statistics.Entities.StatsDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzan
 */
@Controller
@CrossOrigin
@RequestMapping(value = "/admin/statistic")
public class StatisticController {
    
    @Autowired
    private EnquiryService enquiryService;
    @Autowired
    private EnrollmentService enrollmentService;
    @Autowired
    private PaymentService paymentService;
    
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public StatsDTO index() {
        StatsDTO stat = new StatsDTO();
        stat.setPendingEnquiry(enquiryService.countPendingEnquiries().intValue());
        
        float totalEnquiy = enquiryService.getAllEnquiryCount().intValue();
        float notInterestedEnquiries = enquiryService.getNotInterestEnquiry().intValue();
        float bouncePercent = (notInterestedEnquiries / totalEnquiy) * 100;
        stat.setBounceRate(bouncePercent);
        
        stat.setTotalEnrollment(enrollmentService.countTotalEnrollment().intValue());
        stat.setPaymentStat(paymentService.getPaymentStat());
        return stat;
    }      
    
}
