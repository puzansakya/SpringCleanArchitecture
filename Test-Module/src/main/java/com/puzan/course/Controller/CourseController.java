/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.course.Controller;


import com.puzan.course.DTO.CourseDTO;
import com.puzan.course.Service.CourseService;
import com.puzan.entity.Entities.Course;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzan
 */
@Controller
@RequestMapping(value = "/admin/course")
@CrossOrigin(maxAge = 3600)
public class CourseController {

    @Autowired
    private CourseService cs;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("courseList", cs.getAll());
        model.addAttribute("active","course");
        return "course/index";
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<CourseDTO> getAll() {
        ModelMapper mapper = new ModelMapper();
        List<CourseDTO> courseDTOList = new ArrayList<>();
        for (Course c : cs.getAll()) {
            CourseDTO courseDTO = mapper.map(c, CourseDTO.class);
            courseDTOList.add(courseDTO);
        }
        return courseDTOList;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public CourseDTO save(@RequestBody CourseDTO courseDTO) {
        ModelMapper mapper = new ModelMapper();
        Course course = mapper.map(courseDTO, Course.class);
        course.setAddedDate(new Date());
        Course retCourse = cs.insert(course);
        CourseDTO retCourseDTO = mapper.map(retCourse, CourseDTO.class);
        return retCourseDTO;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CourseDTO getById(@PathVariable("id") int id) {
        Course c = cs.getById(id);
        CourseDTO course = new CourseDTO();
        course.setCourseId(c.getCourseId());
        course.setCourseName(c.getCourseName());
        course.setCode(c.getCode());
        course.setFees(c.getFees());
        course.setAddedDate(c.getAddedDate());
        course.setStatus(c.getStatus());
        return course;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestBody CourseDTO c) {
        Course course = new Course();
        course.setCourseId(c.getCourseId());
        course.setCourseName(c.getCourseName());
        course.setCode(c.getCode());
        course.setFees(c.getFees());
        course.setAddedDate(c.getAddedDate());
        course.setStatus(c.isStatus());
        cs.update(course);
        return "[{\"response\":\"success\"}]";
        
    }

}
