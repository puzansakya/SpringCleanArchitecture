/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.course.DAO.Impl;


import com.puzan.course.DAO.CourseDAO;
import com.puzan.entity.Entities.Course;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "CourseDAO")
public class CourseDAOImpl implements CourseDAO {

    @Autowired
    private SessionFactory sf;
    private Session session;
    private Transaction transaction;

    @Override
    public List<Course> getAll() {
        session = sf.openSession();
        List<Course> courseList = session.createQuery("from Course").list();
        session.close();
        return courseList;
    }

    @Override
    public Course insert(Course t) {
        session = sf.openSession();
        transaction = session.beginTransaction();
        session.save(t);
        transaction.commit();
        session.close();
        return t;
    }

    @Override
    public void update(Course t) {
        session = sf.openSession();
        transaction = session.beginTransaction();
        session.update(t);
        transaction.commit();
        session.close();
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Course getById(int id) {
        session = sf.openSession();
        Course course = (Course) session.get(Course.class, id);
        session.close();
        return course;
    }

}
