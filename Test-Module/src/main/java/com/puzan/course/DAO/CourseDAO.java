/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.course.DAO;

import com.puzan.entity.Entities.Course;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface CourseDAO {

    List<Course> getAll();

    Course insert(Course t);

    void delete(int id);

    void update(Course t);

    Course getById(int id);
}
