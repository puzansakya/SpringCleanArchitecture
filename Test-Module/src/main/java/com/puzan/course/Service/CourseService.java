/*
 * Courseo change this license header, choose License Headers in Project Properties.
 * Courseo change this template file, choose Courseools | Courseemplates
 * and open the template in the editor.
 */
package com.puzan.course.Service;



import com.puzan.entity.Entities.Course;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface CourseService {

    List<Course> getAll();

    Course insert(Course t);

    void delete(int id);

    void update(Course t);

    Course getById(int id);

}
