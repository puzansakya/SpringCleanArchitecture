/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.course.Service.Impl;

import com.puzan.course.DAO.CourseDAO;
import com.puzan.course.Service.CourseService;
import com.puzan.entity.Entities.Course;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "CourseService")
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseDAO courseDAO;

    @Override
    public List<Course> getAll() {
        return courseDAO.getAll();
    }

    @Override
    public Course insert(Course t) {
        return courseDAO.insert(t);
    }

    @Override
    public void delete(int id) {
        courseDAO.delete(id);
    }

    @Override
    public void update(Course t) {
        courseDAO.update(t);
    }

    @Override
    public Course getById(int id) {
        return courseDAO.getById(id);
    }

}
