/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.user.Service.Impl;

import com.puzan.entity.Entities.User;
import com.puzan.user.DAO.UserDAO;
import com.puzan.user.Service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "UserService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO ud;

    @Override
    public User getUserByUsername(String username) {
        return ud.getUserByUsername(username);
    }

    @Override
    public List<User> getAll() {
        return ud.getAll();
    }

    @Override
    public User insert(User t) {
        return ud.insert(t);
    }

    @Override
    public void delete(int id) {
        ud.delete(id);
    }

    @Override
    public void update(User t) {
        ud.update(t);
    }

    @Override
    public User getById(int id) {
        return ud.getById(id);
    }

}
