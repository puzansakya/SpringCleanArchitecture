/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.user.Service;

import com.puzan.core.Service.GenericService;
import com.puzan.entity.Entities.User;

/**
 *
 * @author puzan
 */
public interface UserService extends GenericService<User> {

    User getUserByUsername(String username);
}
