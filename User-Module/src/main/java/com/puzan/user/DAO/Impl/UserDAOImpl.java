/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.user.DAO.Impl;

import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.entity.Entities.User;
import com.puzan.user.DAO.UserDAO;

import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "UserDAO")
public class UserDAOImpl extends GenericDAOImpl<User> implements UserDAO {

    @Override
    public User getUserByUsername(String username) {
        session = sessionFactory.openSession();
        List<User> userList = session.createQuery("from User where username = :username")
                .setParameter("username", username)
                .list();
        for (User user : userList) {
            return user;
        }
        return null;

    }

}
