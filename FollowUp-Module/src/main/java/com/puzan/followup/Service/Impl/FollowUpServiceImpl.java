/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.followup.Service.Impl;

import com.puzan.entity.Entities.FollowUps;
import com.puzan.followup.DAO.FollowUpDAO;
import com.puzan.followup.Service.FollowUpService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "FollowUpService")
public class FollowUpServiceImpl implements FollowUpService {

    @Autowired
    private FollowUpDAO fd;

    @Override
    public List<FollowUps> getAll() {
        return fd.getAll();
    }

    @Override
    public FollowUps insert(FollowUps t) {
        return fd.insert(t);
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(FollowUps t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public FollowUps getById(int id) {
        return fd.getById(id);
    }

    @Override
    public Long countFollowUp(int enquiry_id) {
        return fd.countFollowUp(enquiry_id);
    }

    @Override
    public List<FollowUps> getListById(int enquiry_id) {
        return fd.getListById(enquiry_id);
    }

    @Override
    public List<FollowUps> getLatestFollowUp() {
        return fd.getLatestFollowUp();
    }

}
