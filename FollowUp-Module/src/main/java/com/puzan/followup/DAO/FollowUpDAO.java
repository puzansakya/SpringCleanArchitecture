/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.followup.DAO;

import com.puzan.core.DAO.GenericDAO;
import com.puzan.entity.Entities.FollowUps;
import java.util.List;

/**
 *
 * @author puzan
 */
public interface FollowUpDAO extends GenericDAO<FollowUps> {

    Long countFollowUp(int enquiry_id);

    List<FollowUps> getListById(int enquiry_id);

    List<FollowUps> getLatestFollowUp();

}
