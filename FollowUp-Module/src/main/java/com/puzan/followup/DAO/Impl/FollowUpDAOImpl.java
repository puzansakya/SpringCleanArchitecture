/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.followup.DAO.Impl;

import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.entity.Entities.FollowUps;
import com.puzan.followup.DAO.FollowUpDAO;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "FollowUpDAO")
public class FollowUpDAOImpl extends GenericDAOImpl<FollowUps> implements FollowUpDAO {

    @Autowired
    private SessionFactory sf;
    private Session session;
    private Transaction transaction;

    @Override
    public Long countFollowUp(int enquiry_id) {
        session = sf.openSession();
        String hql = "select count(*) from FollowUps  where enquiry_id=:enquiryId";
        Query query = session.createQuery(hql);
        query.setParameter("enquiryId", enquiry_id);
        Long count = (Long) query.uniqueResult();
        session.close();
        return count;
    }

    @Override
    public List<FollowUps> getListById(int enquiry_id) {
        session = sf.openSession();
        String hql = "from FollowUps  where enquiry_id=:enquiryId";
        Query query = session.createQuery(hql);
        query.setParameter("enquiryId", enquiry_id);
        List<FollowUps> followUpList = query.list();
        session.close();
        return followUpList;
    }

    @Override
    public List<FollowUps> getLatestFollowUp() {
        session = sf.openSession();
        String hql = "from FollowUps";
        Query query = session.createQuery(hql);
        query.setMaxResults(5);
        List<FollowUps> followUpList = query.list();
        session.close();
        return followUpList;
    }

}
