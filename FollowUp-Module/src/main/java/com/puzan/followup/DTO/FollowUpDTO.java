/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.followup.DTO;

/**
 *
 * @author puzan
 */
public class FollowUpDTO {

    private int followupId;
    private String followMessage;
    private String created_date;

    //enquiries related flattened data
    private String followup_username;
    private String followup_profile;
    private String firstname;
    private String lastname;

    public int getFollowupId() {
        return followupId;
    }

    public void setFollowupId(int followupId) {
        this.followupId = followupId;
    }

    public String getFollowMessage() {
        return followMessage;
    }

    public void setFollowMessage(String followMessage) {
        this.followMessage = followMessage;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getUsername() {
        return followup_username;
    }

    public void setUsername(String username) {
        this.followup_username = username;
    }

    public String getProfile() {
        return followup_profile;
    }

    public void setProfile(String profile) {
        this.followup_profile = profile;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

}
