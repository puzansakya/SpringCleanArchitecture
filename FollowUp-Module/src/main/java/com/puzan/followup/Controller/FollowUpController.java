/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.followup.Controller;

import com.puzan.entity.Entities.Enquiry;
import com.puzan.entity.Entities.FollowUps;
import com.puzan.followup.DTO.FollowUpDTO;
import com.puzan.followup.Service.FollowUpService;
import com.puzan.user.Service.UserService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzan
 */
@Controller
@CrossOrigin
@RequestMapping(value = "admin/followup")
public class FollowUpController {

    @Autowired
    private FollowUpService fs;

    @Autowired
    private UserService ud;

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return null;
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String create() {
        return null;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public FollowUps save(@RequestBody FollowUps f) {
        FollowUps followUps = new FollowUps();
        followUps.setEnquiryId(f.getEnquiryId());
        followUps.setCreatedDate(f.getCreatedDate());
        followUps.setFollowMessage(f.getFollowMessage());
        followUps.setUsername(ud.getUserByUsername(f.getFollow_username()));
        followUps.setFollowupId(f.getFollowupId());

        FollowUps retFollowUps = fs.insert(followUps);

//        FollowUpDTO dto = new FollowUpDTO();
//        dto.setFollowupId(retFollowUps.getFollowupId());
//        dto.setFollowMessage(retFollowUps.getFollowMessage());
//        dto.setCreated_date(new SimpleDateFormat("yyyy-MM-dd").format(retFollowUps.getCreatedDate()).toString());
//        dto.setUsername(retFollowUps.getUsername().getUsername());
//        dto.setProfile(retFollowUps.getUsername().getProfilePicPath());
//
//        return dto;
        return retFollowUps;
    }

    @RequestMapping(value = "/getFollowUp/{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<FollowUpDTO> getFollowUp(@PathVariable("id") int enquiry_id) {
        List<FollowUpDTO> dtoList = new ArrayList<>();
        for (FollowUps f : fs.getListById(enquiry_id)) {
            FollowUpDTO dto = new FollowUpDTO();
            dto.setFollowupId(f.getFollowupId());
            dto.setFollowMessage(f.getFollowMessage());
            dto.setCreated_date(new SimpleDateFormat("yyyy-MM-dd").format(f.getCreatedDate()).toString());
            dto.setUsername(f.getUsername().getUsername());
            dto.setProfile(f.getUsername().getProfilePicPath());
            dtoList.add(dto);
        }
        return dtoList;
    }

    @RequestMapping(value = "/getCount/{enquiry_id}", method = RequestMethod.GET)
    public @ResponseBody
    String getCount(@PathVariable(value = "enquiry_id") int enquiry_id) {
        return String.valueOf(fs.countFollowUp(enquiry_id));
    }

    @RequestMapping(value = "/getLatestFollowUp", method = RequestMethod.GET)
    @ResponseBody
    public List<FollowUpDTO> getLatestFollowUp() {
        List<FollowUpDTO> dtoList = new ArrayList<>();
        for (FollowUps f : fs.getLatestFollowUp()) {
            FollowUpDTO dto = new FollowUpDTO();
            dto.setFollowupId(f.getFollowupId());
            dto.setFollowMessage(f.getFollowMessage());
            dto.setCreated_date(new SimpleDateFormat("yyyy-MM-dd").format(f.getCreatedDate()).toString());
            dto.setFirstname(f.getEnquiryId().getFirstname());
            dto.setLastname(f.getEnquiryId().getLastname());
            dtoList.add(dto);
        }
        return dtoList;
    }

}
