/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.course.Mapper;

import com.puzan.course.DTO.CourseDTO;
import com.puzan.entity.Entities.Course;


/**
 *
 * @author puzan
 */
public class CourseMapper {

    //convert entity to dto
    public CourseDTO entityToDto(Course c) {
        //create new CourseDTO
        CourseDTO cDTO = new CourseDTO();
        cDTO.setCourseId(c.getCourseId());
        cDTO.setCourseName(c.getCourseName());
        cDTO.setCode(c.getCode());
        cDTO.setAddedDate(c.getAddedDate());
        cDTO.setModifiedDate(c.getModifiedDate());
        cDTO.setFees(c.getFees());
        cDTO.setStatus(c.getStatus());

        return cDTO;

    }

    //convert dto to entity
    public Course DtoToEntity(CourseDTO cDTO) {
        //create new CourseDTO
        Course c = new Course();
        c.setCourseId(cDTO.getCourseId());
        c.setCourseName(cDTO.getCourseName());
        c.setCode(cDTO.getCode());
        c.setAddedDate(cDTO.getAddedDate());
        c.setModifiedDate(cDTO.getModifiedDate());
        c.setFees(cDTO.getFees());
        c.setStatus(cDTO.isStatus());

        return c;
    }

}
