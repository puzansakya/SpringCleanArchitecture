/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enrollstatus.Controller;

import com.puzan.enrollstatus.DTO.EnrollStatusDTO;
import com.puzan.enrollstatus.Service.EnrollStatusService;
import com.puzan.entity.Entities.EnrollStatus;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzansakya
 */
@Controller
@RequestMapping(value = "/admin/enrollStatus")
@CrossOrigin
public class EnrollStatusController {

    @Autowired
    private EnrollStatusService ess;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String index() {
        return "Enrollstatus:index";
    }

    @RequestMapping(value = "/getAll",method = RequestMethod.GET)
    @ResponseBody
    public List<EnrollStatusDTO> getAll() {
        List<EnrollStatusDTO> essDTOList = new ArrayList<>();
        for (EnrollStatus enrollStatus : ess.getAll()) {
            EnrollStatusDTO essDto = new EnrollStatusDTO();
            essDto.setEnrollId(enrollStatus.getEnrollId());
            essDto.setName(enrollStatus.getName());
            essDto.setColor(enrollStatus.getColor());

            essDTOList.add(essDto);

        }

        return essDTOList;

    }
}
