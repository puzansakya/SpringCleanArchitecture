/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enrollstatus.DAO.Impl;

import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.enrollstatus.DAO.EnrollStatusDAO;
import com.puzan.entity.Entities.EnrollStatus;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "EnrollStatusDAO")
public class EnrollStatusDAOImpl extends GenericDAOImpl<EnrollStatus> implements EnrollStatusDAO {

}
