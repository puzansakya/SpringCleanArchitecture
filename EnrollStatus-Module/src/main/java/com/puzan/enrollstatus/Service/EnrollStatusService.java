/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enrollstatus.Service;

import com.puzan.core.Service.GenericService;
import com.puzan.entity.Entities.EnrollStatus;



/**
 *
 * @author puzan
 */
public interface EnrollStatusService extends GenericService<EnrollStatus>{
    
}
