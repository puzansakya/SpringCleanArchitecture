/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.enrollstatus.Service.Impl;

import com.puzan.enrollstatus.DAO.EnrollStatusDAO;
import com.puzan.enrollstatus.Service.EnrollStatusService;
import com.puzan.entity.Entities.EnrollStatus;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "EnrollStatusService")
public class EnrollStatusServiceImpl implements EnrollStatusService {

    @Autowired
    private EnrollStatusDAO esd;

    @Override
    public List<EnrollStatus> getAll() {
        return esd.getAll();
    }

    @Override
    public EnrollStatus insert(EnrollStatus t) {
        return esd.insert(t);
    }

    @Override
    public void delete(int id) {
        esd.delete(id);
    }

    @Override
    public void update(EnrollStatus t) {
        esd.update(t);
    }

    @Override
    public EnrollStatus getById(int id) {
       return esd.getById(id);
    }

}
