/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.calendar.Controller;

import com.puzan.calendar.DTO.EventDTO;
import com.puzan.calendar.Service.EventService;
import com.puzan.entity.Entities.Event;
import com.puzan.helper.DateHelper.DateHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzan
 */
@Controller
@CrossOrigin
@RequestMapping(value = "admin/calendar")
public class CalendarController {

    @Autowired
    private EventService es;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("active", "calendar");
        return "Calendar/index";
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<EventDTO> getAll(Model model) {
        ModelMapper mapper = new ModelMapper();
        List<EventDTO> eventDTOList = new ArrayList<>();
        es.getAll().forEach((event) -> {
            eventDTOList.add(mapper.map(event, EventDTO.class));
        });
        return eventDTOList;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public EventDTO save(@RequestBody EventDTO e) {
        final Date newStartDate = DateHelper.newInstance().stringToDate(e.getStart());
        final Date newEndDate = DateHelper.newInstance().stringToDate(e.getEnd());
        PropertyMap<EventDTO, Event> eventMap = new PropertyMap<EventDTO, Event>() {
            protected void configure() {
                map().setStart(newStartDate);
                map().setEnd(newEndDate);
            }
        };
        ModelMapper mapper = new ModelMapper();
        mapper.addMappings(eventMap);
        Event retEvent = es.insert(mapper.map(e, Event.class));
        EventDTO retEventDTO = mapper.map(retEvent, EventDTO.class);
        retEventDTO.setStart(DateHelper.newInstance().dateToString(newStartDate));
        retEventDTO.setEnd(DateHelper.newInstance().dateToString(newEndDate));
        return retEventDTO;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestBody EventDTO e) {
        Event event = es.getById(e.getId());
        event.setTitle(e.getTitle());
        event.setDescription(e.getDescription());
        event.setColor(e.getColor());
        event.setAllDay(e.isAllDay());

        es.update(event);
        return "[{\"result\":\"success\"}]";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String delete(@PathVariable("id") int id) {
        es.delete(id);
        return "[{\"result\":\"success\"}]";
    }

    @RequestMapping(value = "/getEvent/{id}", method = RequestMethod.GET)
    @ResponseBody
    public EventDTO getById(@PathVariable("id") int id) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(es.getById(id), EventDTO.class);
    }

}
