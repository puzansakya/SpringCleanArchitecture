/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.calendar.DAO.Impl;

import com.puzan.calendar.DAO.EventDAO;
import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.entity.Entities.Event;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "EventDAO")
public class EventDAOImpl extends GenericDAOImpl<Event> implements EventDAO{
    
}
