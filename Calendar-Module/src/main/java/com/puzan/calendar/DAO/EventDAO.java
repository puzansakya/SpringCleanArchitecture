/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.calendar.DAO;

import com.puzan.core.DAO.GenericDAO;
import com.puzan.entity.Entities.Event;

/**
 *
 * @author puzan
 */
public interface EventDAO extends GenericDAO<Event>{
    
}
