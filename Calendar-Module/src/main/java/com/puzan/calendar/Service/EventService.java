/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.calendar.Service;

import com.puzan.core.Service.GenericService;
import com.puzan.entity.Entities.Event;

/**
 *
 * @author puzan
 */
public interface EventService extends GenericService<Event>{
    
}
