/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.calendar.Service.Impl;

import com.puzan.calendar.DAO.EventDAO;
import com.puzan.calendar.Service.EventService;
import com.puzan.entity.Entities.Event;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "EventService")
public class EventServiceImpl implements EventService {

    @Autowired
    private EventDAO ed;

    @Override
    public List<Event> getAll() {
        return ed.getAll();
    }

    @Override
    public Event insert(Event t) {
        return ed.insert(t);
    }

    @Override
    public void delete(int id) {
        ed.delete(id);
    }

    @Override
    public void update(Event t) {
        ed.update(t);
    }

    @Override
    public Event getById(int id) {
        return ed.getById(id);
    }

}
