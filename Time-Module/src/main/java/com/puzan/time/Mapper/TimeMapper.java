/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.time.Mapper;

import com.puzan.entity.Entities.Time;
import com.puzan.time.DTO.TimeDTO;

/**
 *
 * @author puzan
 */
public class TimeMapper {
    //convert entity to dto

    public TimeDTO entityToDto(Time t) {
        //create new TimeDTO
        TimeDTO tDto = new TimeDTO();
        tDto.setTimeId(t.getTimeId());
        tDto.setTime(t.getTime());

        return tDto;

    }

    //convert dto to entity
    public Time DtoToEntity(TimeDTO tDTO) {
        Time t = new Time();               
        t.setTimeId(tDTO.getTimeId());
        t.setTime(tDTO.getTime());

        return t;
       
    }
}
