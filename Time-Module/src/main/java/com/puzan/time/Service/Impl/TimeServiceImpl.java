/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.time.Service.Impl;

import com.puzan.entity.Entities.Time;
import com.puzan.time.DAO.TimeDAO;
import com.puzan.time.Service.TimeService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author puzan
 */
@Service(value = "TimeService")
public class TimeServiceImpl implements TimeService {

    @Autowired
    private TimeDAO td;

    @Override
    public List<Time> getAll() {
        return td.getAll();
    }

    @Override
    public Time insert(Time t) {
        return td.insert(t);
    }

    @Override
    public void delete(int id) {
        td.delete(id);
    }

    @Override
    public void update(Time t) {
        td.update(t);
    }

    @Override
    public Time getById(int id) {
        return td.getById(id);
    }

}
