/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.time.Service;

import com.puzan.core.Service.GenericService;
import com.puzan.entity.Entities.Time;

/**
 *
 * @author puzan
 */
public interface TimeService extends GenericService<Time> {
    
}
