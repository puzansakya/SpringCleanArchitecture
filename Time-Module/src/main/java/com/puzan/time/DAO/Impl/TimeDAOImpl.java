/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.time.DAO.Impl;


import com.puzan.core.DAO.Impl.GenericDAOImpl;
import com.puzan.entity.Entities.Time;
import com.puzan.time.DAO.TimeDAO;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzan
 */
@Repository(value = "TimeDAO")
public class TimeDAOImpl extends GenericDAOImpl<Time> implements TimeDAO {

}
