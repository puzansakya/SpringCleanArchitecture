/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzan.time.Controller;

import com.puzan.entity.Entities.Time;
import com.puzan.time.DTO.TimeDTO;
import com.puzan.time.Service.TimeService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author puzansakya
 */
@Controller
@CrossOrigin
@RequestMapping(value = "/admin/time")
public class TimeController {

    @Autowired
    private TimeService ts;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String index() {
        return "time:index";
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<TimeDTO> getAll() {
        List<TimeDTO> timeList = new ArrayList<>();
        for (Time time : ts.getAll()) {
            TimeDTO tDto = new TimeDTO();
            tDto.setTimeId(time.getTimeId());
            tDto.setTime(time.getTime());

            timeList.add(tDto);
        }
        return timeList;
    }
}
